grammar FirstTyr;

// Parser rules
file: functionDecl EOF;

functionDecl:
    TYPE? ID ASSIGN LPAREN params? RPAREN LBRACE blockBody? returnStatement? RBRACE
;

params: param (COMMA param)*;

param: ID COLON TYPE;

blockBody: (statement)+;

statement:
    assignment
    | functionDecl
    | functionCall
    | returnStatement
    | ifStatement
;

assignment:
    MUTABLE? ID ASSIGN (identifier | expression)
    | ID ASSIGN arrayIndexing
;

returnStatement: RETURN expression;

ifStatement:
    IF LPAREN expression RPAREN LBRACE blockBody? RBRACE elseIfPart* elsePart?
;

elseIfPart:
    ELSEIF LPAREN expression RPAREN LBRACE blockBody? RBRACE
;

elsePart: ELSE LBRACE blockBody? RBRACE;

expression: expression (EQ | NE) comparison | comparison;

comparison: comparison (LT | LE | GT | GE) addition | addition;

addition: addition (ADD | SUB) multiplication | multiplication;

multiplication: multiplication (MUL | DIV) unary | unary;

unary: SUB unary | typeCast;

typeCast: LPAREN TYPE RPAREN typeCast | atom;

atom:
    LPAREN expression RPAREN
    | functionCall
    | arrayDeclaration
    | arrayIndexing
    | identifier
    | literal
;

arrayDeclaration:
    LBRACK (expression (COMMA expression)*)? RBRACK
;

arrayIndexing: identifier LBRACK expression RBRACK;

literal: INT | FLOAT | BOOLEAN | STRING;

arg: identifier | (ID ASSIGN expression);

args: arg (COMMA arg)*;

functionCall: ID LPAREN args? RPAREN;

identifier: ID;

// Lexer rules
COMMENT: '//' ~[\r\n]* -> channel(HIDDEN);

TYPE: ('float' | 'int' | 'bool' | 'str');
MUTABLE: 'mut';

LBRACE: '{';
RBRACE: '}';
LBRACK: '[';
RBRACK: ']';
COLON: ':';
COMMA: ',';
LPAREN: '(';
RPAREN: ')';
ASSIGN: '=';
MUL: '*';
DIV: '/';
ADD: '+';
SUB: '-';
EQ: '==';
NE: '!=';
LT: '<';
LE: '<=';
GT: '>';
GE: '>=';
RETURN: 'return';
IF: 'if';
ELSEIF: 'else if';
ELSE: 'else';

INT: [0] | [1-9][0-9]*;
FLOAT: [0] '.' [0-9]+ | [1-9][0-9]* '.' [0-9]+;
BOOLEAN: ('true' | 'false');
STRING: '"' (~["\r\n])* '"';

ID: [a-zA-Z_][a-zA-Z0-9_]*;

WS: [ \t\r\n]+ -> channel(HIDDEN);