#include "FileHandler.h"

std::tuple<int, int, int> parse_semver(const std::string &filename)
{
    int major, minor, patch;
    sscanf(filename.c_str(), "%d.%d.%d", &major, &minor, &patch);
    return std::make_tuple(major, minor, patch);
}

void parse_file(const std::string &filename, std::shared_ptr<RootNode> astRoot, bool exitOnError = false)
{
    std::cout << "Parsing file: " << filename << "\n\n";

    // Read the content of the file
    std::ifstream file(filename);
    std::stringstream buffer;
    buffer << file.rdbuf();
    std::string input_str = buffer.str();

    // Get the full file path
    std::filesystem::path filePath = std::filesystem::absolute(filename);
    std::string fullFilePath = filePath.string();

    // Compile the file
    antlr4::ANTLRInputStream input(input_str);
    FirstTyrLexer lexer(&input);
    antlr4::CommonTokenStream tokens(&lexer);
    FirstTyrParser parser(&tokens);
    antlr4::tree::ParseTree *fileParseTree = parser.file();

    // Set up the visitor
    auto visitor = std::make_unique<CustomFirstTyrVisitor>(parser, tokens, astRoot);

    // Populate the astRoot with the AST
    visitor->populateAST(fileParseTree, filename, fullFilePath);

    // Check for ANTLR parser errors
    if (visitor->getParserErrorHandler().hasErrors())
    {
        const auto &errors = visitor->getParserErrorHandler().getErrors();

        // Print out any errors
        std::cout << "\nErrors for: " << filename << "\n";
        for (const auto &error : errors)
        {
            // Print out the full path, line, and column of the error:
            // <full file path>:<line>:<column>: <error message>
            std::cout << fullFilePath << ":" << error.getLine() << ":" << error.getColumn() << ": " << error.getMessage() << "\n";
        }
        if (exitOnError)
        {
            std::cerr << "Exiting due to parser errors in file " + filename + "\n";
            exit(EXIT_FAILURE);
        }
    }
}

void run_tests(std::tuple<int, int, int> compilerVersion)
{
    std::string tests_folder = "../tests"; // Path to the tests folder

    // Read the test file names into a vector
    std::vector<std::filesystem::path> test_files;
    for (const auto &entry : std::filesystem::directory_iterator(tests_folder))
    {
        const std::filesystem::path &test_file = entry.path();
        if (test_file.extension().string() == ".ftyr")
        {
            test_files.push_back(test_file);
        }
    }

    // Sort the test files based on their SemVer values
    std::sort(test_files.begin(), test_files.end(), [](const auto &a, const auto &b)
              { return parse_semver(a.stem()) < parse_semver(b.stem()); });

    // Run tests on the sorted files
    for (const auto &test_file : test_files)
    {
        std::string test_filename = test_file.filename();

        // Parse the version from the test filename
        std::tuple<int, int, int> test_version = parse_semver(test_file.stem());

        // Skip if the test version is greater than the compiler version
        if (test_version > compilerVersion)
        {
            std::cout << "\n\nSkipping test for: " << test_filename << " (requires newer compiler version)\n";
            continue;
        }

        // Start test
        std::cout << "\n\nStarting test for: " << test_filename << "\n";

        std::shared_ptr<RootNode> ast = std::make_shared<RootNode>();
        parse_file(test_file, ast);

        // Generate LLVM IR from AST
        ASTVisitor generator(*ast);
        std::unique_ptr<llvm::Module> module = generator.generate();

        // Print the generated LLVM IR for each test
        std::cout << "\nGenerated LLVM IR for " << test_filename << ":\n";
        module->print(llvm::outs(), nullptr);
        std::cout << '\n';
    }
}

void compile_file(const std::string &filename)
{
    std::cout << "Compiling file: " << filename << "\n";

    std::shared_ptr<RootNode> ast = std::make_shared<RootNode>();
    parse_file(filename, ast, true);

    // Generate LLVM IR from AST
    ASTVisitor generator(*ast);
    std::unique_ptr<llvm::Module> module = generator.generate();

    // Print the generated LLVM IR
    std::cout << "\nGenerated LLVM IR:\n";
    module->print(llvm::outs(), nullptr);

    // Now we have a .o file which is a binary, next we use clang to link it into an executable.
    // Remove extension from filename
    size_t lastindex = filename.find_last_of(".");
    std::string rawname = filename.substr(0, lastindex);

    std::string llFilename = rawname + ".ll";
    std::error_code EC;
    llvm::raw_fd_ostream dest(llFilename, EC, llvm::sys::fs::OF_None);

    if (EC)
    {
        std::cerr << "Could not open file: " << EC.message();
        return;
    }

    module->print(dest, nullptr);

    dest.flush();
}
