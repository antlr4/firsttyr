#include "ScopeStack.h"

void ScopeStack::pushScope()
{
    // std::cout << "Pushing scope" << std::endl;
    scopes.push_back({});
}

void ScopeStack::popScope()
{
    if (!scopes.empty())
    {
        // std::cout << "Popping scope" << std::endl;
        scopes.pop_back();
    }
    else
    {
        // std::cout << "Cannot pop scope, stack is empty" << std::endl;
    }
}

void ScopeStack::addSymbol(const Symbol &symbol)
{
    if (!scopes.empty())
    {
        // std::cout << "Adding symbol " << symbol.name << std::endl;
        scopes.back()[symbol.name] = symbol;
    }
    else
    {
        // std::cout << "Cannot add symbol '" << symbol.name << "', stack is empty" << std::endl;
    }
}

Symbol *ScopeStack::getSymbol(const std::string &name)
{
    // std::cout << "Getting symbol '" << name << "'" << std::endl;
    for (auto it = scopes.rbegin(); it != scopes.rend(); ++it)
    {
        if (it->count(name))
        {
            // std::cout << "Found symbol '" << name << "'" << std::endl;
            return &(*it)[name];
        }
    }
    // std::cout << "Symbol '" << name << "' not found" << std::endl;
    return nullptr;
}

Symbol *ScopeStack::getCurrentScopeSymbol(const std::string &name)
{
    // std::cout << "Getting symbol '" << name << "' from current scope" << std::endl;
    if (!scopes.empty() && scopes.back().count(name))
    {
        // std::cout << "Found symbol '" << name << "'" << std::endl;
        return &scopes.back()[name];
    }
    return nullptr;
}

void ScopeStack::listSymbols() const
{
    // std::cout << "Listing symbols" << std::endl;
    for (const auto &scope : scopes)
    {
        for (const auto &symbol : scope)
        {
            // std::cout << symbol.first << std::endl;
        }
    }
}