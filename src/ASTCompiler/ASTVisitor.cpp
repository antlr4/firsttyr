#include "ASTVisitor.h"

ASTVisitor::ASTVisitor(const RootNode &root) : builder(context), root(root)
{
    // Create a new LLVM module
    module = std::make_unique<llvm::Module>("FirstTyrModule", context);

    // Set the target triple
    module->setTargetTriple("arm64-apple-macosx14.0.0");
}

std::unique_ptr<llvm::Module> ASTVisitor::generate()
{
    // Add root scope
    scopeStack.pushScope();

    // Declare each required standard library function
    for (const auto &standardFunction : root.getStandardFunctions())
    {
        // If the standard function name is print, declare the function
        if (standardFunction == "print")
        {
            // TODO: Write function in FirstTyr and import it here

            // Fetch the function
            llvm::Function *printFunc = getPrintfFunc();

            // Register the symbol
            Symbol printSymbol;
            printSymbol.name = "print";
            printSymbol.type = SymbolType::Function;
            printSymbol.value = printFunc;
            printSymbol.isFunctionParameter = false;
            printSymbol.functionReturnType = llvm::Type::getVoidTy(context);
            printSymbol.functionParameterIndices = {{"text", 0}};
            printSymbol.functionParameterTypes = {{"text", SymbolType::String}};
            scopeStack.addSymbol(printSymbol);
        }
    }

    // Visit each file in the AST
    for (const auto &file : root.getFiles())
    {
        visit(*file);
    }

    // Verify the generated code to ensure its correctness
    llvm::verifyModule(*module, &llvm::errs());

    return std::move(module);
}

TypedValue ASTVisitor::visit(const AdditionNode &node)
{
    // Get the left and right operands
    TypedValue left = visit(*node.getLeftOperand());
    TypedValue right = visit(*node.getRightOperand());

    // Set up left and right llvm types and values
    llvm::Type *leftType = left.value->getType();
    llvm::Type *rightType = right.value->getType();
    llvm::Value *leftValue = left.value;
    llvm::Value *rightValue = right.value;

    // Type promotion for mixed arithmetic
    if (leftType->isIntegerTy() && rightType->isFloatTy())
    {
        leftValue = builder.CreateSIToFP(leftValue, llvm::Type::getFloatTy(context), "int_to_float");
        leftType = llvm::Type::getFloatTy(context);
    }
    else if (leftType->isFloatTy() && rightType->isIntegerTy())
    {
        rightValue = builder.CreateSIToFP(rightValue, llvm::Type::getFloatTy(context), "int_to_float");
        rightType = llvm::Type::getFloatTy(context);
    }

    // Integer arithmetic
    if (leftType->isIntegerTy() && rightType->isIntegerTy())
    {
        if (node.getOperatorType() == AdditionOperator::ADD)
        {
            return {SymbolType::Integer, builder.CreateAdd(leftValue, rightValue, "add")};
        }
        else if (node.getOperatorType() == AdditionOperator::SUB)
        {
            return {SymbolType::Integer, builder.CreateSub(leftValue, rightValue, "sub")};
        }
    }
    // Floating-point arithmetic
    else if (leftType->isFloatTy() && rightType->isFloatTy())
    {
        if (node.getOperatorType() == AdditionOperator::ADD)
        {
            return {SymbolType::Float, builder.CreateFAdd(leftValue, rightValue, "fadd")};
        }
        else if (node.getOperatorType() == AdditionOperator::SUB)
        {
            return {SymbolType::Float, builder.CreateFSub(leftValue, rightValue, "fsub")};
        }
    }
    // String concatenation
    else if (leftType->isPointerTy() && rightType->isPointerTy())
    {
        // Allocate memory for the concatenated string
        llvm::Value *leftLength = builder.CreateCall(getStrlenFunc(), leftValue, "strlen");
        llvm::Value *rightLength = builder.CreateCall(getStrlenFunc(), rightValue, "strlen");
        llvm::Value *mallocSize = builder.CreateAdd(builder.CreateAdd(leftLength, rightLength), builder.getInt64(1));
        llvm::Value *concatenatedString = builder.CreateCall(getMallocFunc(), mallocSize, "malloc");

        // Copy the first string into the allocated memory
        std::vector<llvm::Value *> strcpyArgs;
        strcpyArgs.push_back(concatenatedString);
        strcpyArgs.push_back(leftValue);
        builder.CreateCall(getStrcpyFunc(), strcpyArgs, "strcpy");

        // Append the second string to the first string
        std::vector<llvm::Value *> strcatArgs;
        strcatArgs.push_back(concatenatedString);
        strcatArgs.push_back(rightValue);
        builder.CreateCall(getStrcatFunc(), strcatArgs, "strcat");

        return {SymbolType::String, concatenatedString};
    }

    // If we reach here, it means we have an unimplemented case
    std::cerr << "Error: Unimplemented arithmetics for addition node\n";
    std::cout << node.getTokenText() << '\n';
    exit(EXIT_FAILURE);
}

TypedValue ASTVisitor::visit(const ArrayNode &node)
{
    // Get the elements of the array
    const auto &elements = node.getElements();

    // Get the type of the array elements
    NodeType nodeType = node.getType();

    // Print the type of the array elements
    std::cout << "ArrayNode TypeString from Nodetype: " << getTypeStringFromNodeType(nodeType) << '\n';

    // Get the LLVM type from the node type
    llvm::Type *elementType = getLLVMTypeFromNodeType(nodeType);

    // Create an array type of the appropriate size
    llvm::ArrayType *arrayType = llvm::ArrayType::get(elementType, elements.size());

    // Allocate memory for the array
    llvm::Value *arrayPtr = builder.CreateAlloca(arrayType);

    // Visit each element in the array
    for (size_t i = 0; i < elements.size(); ++i)
    {
        // Use dynamic_cast to ensure the node is a LiteralNode
        auto elementNode = elements[i].get();
        if (elementNode)
        {
            // Visit the value of the element
            TypedValue elementValue;

            if (nodeType == NodeType::IntLiteralNode)
            {
                elementValue = visit(*std::dynamic_pointer_cast<IntLiteralNode>(elementNode->getExpression()));
            }
            else if (nodeType == NodeType::FloatLiteralNode)
            {
                elementValue = visit(*std::dynamic_pointer_cast<FloatLiteralNode>(elementNode->getExpression()));
            }
            else if (nodeType == NodeType::BooleanLiteralNode)
            {
                elementValue = visit(*std::dynamic_pointer_cast<BooleanLiteralNode>(elementNode->getExpression()));
            }
            else if (nodeType == NodeType::StringLiteralNode)
            {
                elementValue = visit(*std::dynamic_pointer_cast<StringLiteralNode>(elementNode->getExpression()));
            }

            // Create an index for the element
            llvm::Value *index = llvm::ConstantInt::get(llvm::Type::getInt32Ty(context), i);

            // Get a pointer to the element in the array
            llvm::Value *elementPtr = builder.CreateGEP(arrayType, arrayPtr, {builder.getInt32(0), index});

            // Store the element value in the array
            builder.CreateStore(elementValue.value, elementPtr);
        }
        else
        {
            // Handle error: array elements must be LiteralNodes
            throw std::runtime_error("Array elements must be LiteralNodes");
        }
    }

    // Return the pointer to the array
    return TypedValue{SymbolType::Array, arrayPtr};
}

TypedValue ASTVisitor::visit(const ArrayIndexingNode &node)
{
    // Visit the array expression to get the pointer to the array
    TypedValue arrayValue = visit(*node.getArray());
    if (arrayValue.type != SymbolType::Array)
    {
        throw std::runtime_error("Array indexing applied to non-array type");
    }

    // Visit the index expression to get the index
    TypedValue indexValue = visit(*node.getIndex());
    if (indexValue.type != SymbolType::Integer)
    {
        throw std::runtime_error("Array index is not an integer");
    }

    // Create an index list
    std::vector<llvm::Value *> indexList = {builder.getInt32(0), indexValue.value};

    // Use the LLVM CreateGEP function to get a pointer to the element at the given index in the array
    llvm::Value *elementPtr = builder.CreateGEP(arrayValue.value->getType(), arrayValue.value, indexList);

    // Get the symbol from the symbol table
    Symbol *symbol = scopeStack.getSymbol(node.getArray()->getIdentifier());
    if (symbol == nullptr)
    {
        throw std::runtime_error("Array not found in symbol table");
    }

    std::cout << "Array name: " << symbol->name << '\n';

    // Get the LLVM type from the symbol type
    llvm::Type *elementType = getLLVMTypeFromSymbolType(symbol->type);

    std::cout << "Array type: " << getTypeStringFromSymbolType(symbol->type) << '\n';

    // Load the value at the obtained pointer
    llvm::Value *elementValue = builder.CreateLoad(elementType, elementPtr);

    // Return the loaded value
    return TypedValue{symbol->type, elementValue};
}

Symbol ASTVisitor::visit(const ArgNode &node)
{
    // Get the name of the argument
    std::string argName = node.getIdentifier();

    // Visit the value of the argument
    TypedValue value = visit(*node.getExpression());

    // Create a new symbol for the argument
    Symbol arg;
    arg.name = argName;
    arg.type = value.type;
    arg.isMutable = false;
    arg.value = value.value;

    return arg;
}

std::vector<Symbol> ASTVisitor::visit(const ArgsNode &node)
{
    std::vector<Symbol> args;
    if (!node.getArgs().empty())
    {
        for (const auto &argNode : node.getArgs())
        {
            args.push_back(visit(*argNode));
        }
    }
    return args;
}

void ASTVisitor::visit(const AssignmentNode &node)
{
    // Get the identifier
    auto identifier = node.getIdentifier();

    // Get the assignment type
    auto assignmentType = node.getAssignmentType();

    // Get the expression
    auto valueNode = node.getValue();

    // Set up the expression value
    TypedValue expressionValue;

    // Check if the expression is an identifier or expression
    if (valueNode->getNodeType() == NodeType::ExpressionNode)
    {
        expressionValue = visit(*std::dynamic_pointer_cast<ExpressionNode>(valueNode));
    }
    else if (valueNode->getNodeType() == NodeType::IdentifierNode)
    {
        expressionValue = visit(*std::dynamic_pointer_cast<IdentifierNode>(valueNode));
    }
    else
    {
        throw std::runtime_error("Unknown value type: " + valueNode->getTokenText());
    }

    // Check if the identifier already exists in the current scope
    auto existingSymbol = scopeStack.getCurrentScopeSymbol(identifier);
    if (existingSymbol)
    {
        // Check if the symbol is mutable
        if (!existingSymbol->isMutable)
        {
            std::cerr << "Error: Identifier '" << identifier << "' is immutable." << std::endl;
            std::cout << node.getTokenText() << '\n';
            exit(EXIT_FAILURE);
        }
        else
        {
            // Check if new assignment attempts to set symbol to mutable
            // (For existing symbols, the assignment type should always be immutable)
            if (assignmentType == AssignmentType::Mutable)
            {
                std::cerr << "Error: Identifier reassignment of '" << identifier << "' should not mark mutability." << std::endl;
                std::cout << node.getTokenText() << '\n';
                exit(EXIT_FAILURE);
            }
        }
        // Check if the new value's type matches the existing symbol's type
        if (existingSymbol->type != expressionValue.type)
        {
            std::cerr << "Error: Identifier '" << identifier << "' is of type '" << getTypeStringFromSymbolType(existingSymbol->type) << "', but attempted to assign value of type '" << getTypeStringFromSymbolType(expressionValue.type) << "'." << std::endl;
            std::cout << node.getTokenText() << '\n';
            exit(EXIT_FAILURE);
        }
        else
        {
            // Update the symbol's value
            existingSymbol->value = expressionValue.value;
        }

        // Save the symbol to the current scope
        scopeStack.addSymbol(*existingSymbol);
    }
    else
    {
        // Create a new symbol
        Symbol symbol;
        symbol.name = identifier;
        symbol.type = expressionValue.type;
        symbol.isMutable = assignmentType == AssignmentType::Mutable;
        symbol.value = expressionValue.value;
        symbol.isFunctionParameter = false;

        // Add the symbol to the current scope
        scopeStack.addSymbol(symbol);
    }
}

void ASTVisitor::visit(const BlockBodyNode &node)
{
    // Visit each statement in the block body
    for (const auto &statement : node.getStatements())
    {
        visit(*statement);
    }
}

TypedValue ASTVisitor::visit(const ComparisonNode &node)
{
    // Get the left and right operands
    TypedValue left = visit(*node.getLeftOperand());
    TypedValue right = visit(*node.getRightOperand());

    // Set up left and right llvm types and values
    llvm::Type *leftType = left.value->getType();
    llvm::Type *rightType = right.value->getType();
    llvm::Value *leftValue = left.value;
    llvm::Value *rightValue = right.value;

    // Type promotion for mixed comparison
    if (leftType->isIntegerTy() && rightType->isFloatTy())
    {
        leftValue = builder.CreateSIToFP(leftValue, llvm::Type::getFloatTy(context), "int_to_float");
        leftType = llvm::Type::getFloatTy(context);
    }
    else if (leftType->isFloatTy() && rightType->isIntegerTy())
    {
        rightValue = builder.CreateSIToFP(rightValue, llvm::Type::getFloatTy(context), "int_to_float");
        rightType = llvm::Type::getFloatTy(context);
    }

    // Integer comparison
    if (leftType->isIntegerTy() && rightType->isIntegerTy())
    {
        switch (node.getOperatorType())
        {
        case ComparisonOperator::LT:
            return {SymbolType::Boolean, builder.CreateICmpSLT(leftValue, rightValue, "lt")};
        case ComparisonOperator::LE:
            return {SymbolType::Boolean, builder.CreateICmpSLE(leftValue, rightValue, "le")};
        case ComparisonOperator::GT:
            return {SymbolType::Boolean, builder.CreateICmpSGT(leftValue, rightValue, "gt")};
        case ComparisonOperator::GE:
            return {SymbolType::Boolean, builder.CreateICmpSGE(leftValue, rightValue, "ge")};
        }
    }
    // Floating-point comparison
    else if (leftType->isFloatTy() && rightType->isFloatTy())
    {
        switch (node.getOperatorType())
        {
        case ComparisonOperator::LT:
            return {SymbolType::Boolean, builder.CreateFCmpOLT(leftValue, rightValue, "flt")};
        case ComparisonOperator::LE:
            return {SymbolType::Boolean, builder.CreateFCmpOLE(leftValue, rightValue, "fle")};
        case ComparisonOperator::GT:
            return {SymbolType::Boolean, builder.CreateFCmpOGT(leftValue, rightValue, "fgt")};
        case ComparisonOperator::GE:
            return {SymbolType::Boolean, builder.CreateFCmpOGE(leftValue, rightValue, "fge")};
        }
    }

    // If we reach here, it means we have an unimplemented case
    std::cerr << "Error: Unimplemented comparison for comparison node\n";
    std::cout << node.getTokenText() << '\n';
    exit(EXIT_FAILURE);
}

TypedValue ASTVisitor::visit(const EqualityNode &node)
{
    // Get the left and right operands
    TypedValue left = visit(*node.getLeftOperand());
    TypedValue right = visit(*node.getRightOperand());

    // Set up left and right llvm types and values
    llvm::Type *leftType = left.value->getType();
    llvm::Type *rightType = right.value->getType();
    llvm::Value *leftValue = left.value;
    llvm::Value *rightValue = right.value;

    // Type promotion for mixed comparison
    if (leftType->isIntegerTy() && rightType->isFloatTy())
    {
        leftValue = builder.CreateSIToFP(leftValue, llvm::Type::getFloatTy(context), "int_to_float");
        leftType = llvm::Type::getFloatTy(context);
    }
    else if (leftType->isFloatTy() && rightType->isIntegerTy())
    {
        rightValue = builder.CreateSIToFP(rightValue, llvm::Type::getFloatTy(context), "int_to_float");
        rightType = llvm::Type::getFloatTy(context);
    }

    // Integer comparison
    if (leftType->isIntegerTy() && rightType->isIntegerTy())
    {
        if (node.getOperatorType() == EqualityOperator::EQ)
        {
            return {SymbolType::Boolean, builder.CreateICmpEQ(leftValue, rightValue, "eq")};
        }
        else if (node.getOperatorType() == EqualityOperator::NE)
        {
            return {SymbolType::Boolean, builder.CreateICmpNE(leftValue, rightValue, "neq")};
        }
    }
    // Floating-point comparison
    else if (leftType->isFloatTy() && rightType->isFloatTy())
    {
        if (node.getOperatorType() == EqualityOperator::EQ)
        {
            return {SymbolType::Boolean, builder.CreateFCmpOEQ(leftValue, rightValue, "feq")};
        }
        else if (node.getOperatorType() == EqualityOperator::EQ)
        {
            return {SymbolType::Boolean, builder.CreateFCmpONE(leftValue, rightValue, "fneq")};
        }
    }
    // String comparison
    else if (leftType->isPointerTy() && rightType->isPointerTy())
    {
        // Get strcmp function
        llvm::Function *strcmpFunc = getStrcmpFunc();

        // Call strcmp function
        llvm::Value *strcmpResult = builder.CreateCall(strcmpFunc, {leftValue, rightValue}, "strcmp");

        // Compare strcmp result with 0
        switch (node.getOperatorType())
        {
        case EqualityOperator::EQ:
            return {SymbolType::Boolean, builder.CreateICmpEQ(strcmpResult, builder.getInt32(0), "eq")};
        case EqualityOperator::NE:
            return {SymbolType::Boolean, builder.CreateICmpNE(strcmpResult, builder.getInt32(0), "ne")};
        }
    }

    // If we reach here, it means we have an unimplemented case
    std::cerr << "Error: Unimplemented comparison for equality node\n";
    std::cout << node.getTokenText() << '\n';
    exit(EXIT_FAILURE);
}

TypedValue ASTVisitor::visit(const ExpressionNode &node)
{
    auto expressionNode = node.getExpression();

    if (expressionNode->getNodeType() == NodeType::AdditionNode)
    {
        return visit(*std::dynamic_pointer_cast<AdditionNode>(expressionNode));
    }
    else if (expressionNode->getNodeType() == NodeType::MultiplicationNode)
    {
        return visit(*std::dynamic_pointer_cast<MultiplicationNode>(expressionNode));
    }
    else if (expressionNode->getNodeType() == NodeType::UnaryNode)
    {
        return visit(*std::dynamic_pointer_cast<UnaryNode>(expressionNode));
    }
    else if (expressionNode->getNodeType() == NodeType::TypeCastNode)
    {
        return visit(*std::dynamic_pointer_cast<TypeCastNode>(expressionNode));
    }
    else if (expressionNode->getNodeType() == NodeType::IdentifierNode)
    {
        return visit(*std::dynamic_pointer_cast<IdentifierNode>(expressionNode));
    }
    else if (expressionNode->getNodeType() == NodeType::EqualityNode)
    {
        return visit(*std::dynamic_pointer_cast<EqualityNode>(expressionNode));
    }
    else if (expressionNode->getNodeType() == NodeType::ComparisonNode)
    {
        return visit(*std::dynamic_pointer_cast<ComparisonNode>(expressionNode));
    }
    else if (expressionNode->getNodeType() == NodeType::IntLiteralNode)
    {
        return visit(*std::dynamic_pointer_cast<IntLiteralNode>(expressionNode));
    }
    else if (expressionNode->getNodeType() == NodeType::FloatLiteralNode)
    {
        return visit(*std::dynamic_pointer_cast<FloatLiteralNode>(expressionNode));
    }
    else if (expressionNode->getNodeType() == NodeType::BooleanLiteralNode)
    {
        return visit(*std::dynamic_pointer_cast<BooleanLiteralNode>(expressionNode));
    }
    else if (expressionNode->getNodeType() == NodeType::StringLiteralNode)
    {
        return visit(*std::dynamic_pointer_cast<StringLiteralNode>(expressionNode));
    }
    else if (expressionNode->getNodeType() == NodeType::FunctionCallNode)
    {
        return visit(*std::dynamic_pointer_cast<FunctionCallNode>(expressionNode));
    }
    else if (expressionNode->getNodeType() == NodeType::ArrayNode)
    {
        return visit(*std::dynamic_pointer_cast<ArrayNode>(expressionNode));
    }
    else if (expressionNode->getNodeType() == NodeType::ArrayIndexingNode)
    {
        return visit(*std::dynamic_pointer_cast<ArrayIndexingNode>(expressionNode));
    }
    else
    {
        throw std::runtime_error("Unknown expression type: " + expressionNode->getTokenText());
    }
}

void ASTVisitor::visit(const FileNode &node)
{
    // Create a new scope for the file
    scopeStack.pushScope();

    // Visit each function declaration in the file
    for (const auto &functionDecl : node.getFunctionDecls())
    {
        visit(*functionDecl);
    }

    // Pop the file scope after visiting all children
    scopeStack.popScope();
}

TypedValue ASTVisitor::visit(const FunctionCallNode &node)
{

    // Get the function name
    std::string functionName = node.getFunctionName();

    // Look for the function symbol
    Symbol *functionSymbol = getSymbol(functionName);

    if (!functionSymbol || functionSymbol->type != SymbolType::Function)
    {
        std::cerr << "Error: Function '" << functionName << "' not found." << std::endl;
        std::cout << node.getTokenText() << '\n';
        exit(EXIT_FAILURE);
    }

    std::vector<llvm::Value *> orderedArgs(functionSymbol->functionParameterIndices.size(), nullptr);
    std::map<std::string, TypedValue> argValues;

    // If there are arguments, visit them
    const std::shared_ptr<ArgsNode> argsNode = node.getArguments();
    if (argsNode != nullptr)
    {
        std::vector<Symbol> args = visit(*argsNode);

        // // Print the argument names
        // std::cout << "Argument names: ";
        // for (const auto &arg : args)
        // {
        //     std::cout << arg.name << ' ';
        // }
        // std::cout << '\n';

        // // Print the function parameter names
        // std::cout << "Function parameter names: ";
        // for (const auto &param : functionSymbol->functionParameterIndices)
        // {
        //     std::cout << param.first << ' ';
        // }
        // std::cout << '\n';

        // Iterate over the arguments
        for (const auto &arg : args)
        {
            // Initialize argument name and value
            std::string argName = arg.name;

            if (functionSymbol->functionParameterIndices.count(argName) > 0)
            {
                argValues.emplace(argName, TypedValue{arg.type, arg.value});
            }
            else
            {
                std::cerr << "Error: Unknown argument name: " << argName << std::endl;
                std::cout << node.getTokenText() << '\n';
                exit(EXIT_FAILURE);
            }
        }
    }

    // Check each parameter in the function declaration
    for (const auto &param : functionSymbol->functionParameterIndices)
    {
        // If the argument was not provided, throw an error
        if (argValues.count(param.first) == 0)
        {
            std::cerr << "Error: Argument '" << param.first << "' not provided in function call." << std::endl;
            std::cout << node.getTokenText() << '\n';
            exit(EXIT_FAILURE);
        }

        // Check the argument type against the expected type
        TypedValue argValue = argValues.at(param.first);
        SymbolType expectedType = functionSymbol->functionParameterTypes[param.first];
        if (argValue.type != expectedType)
        {
            std::cerr << "Error: Argument '" << param.first << "' has type " << getTypeStringFromSymbolType(argValue.type)
                      << ", but function expects type " << getTypeStringFromSymbolType(expectedType) << "." << std::endl;
            std::cout << node.getTokenText() << '\n';
            exit(EXIT_FAILURE);
        }

        // Add the argument to orderedArgs in the correct position
        orderedArgs[param.second] = argValue.value;
    }

    // Generate the appropriate LLVM IR for the function call
    llvm::Function *function = static_cast<llvm::Function *>(functionSymbol->value);
    llvm::CallInst *call = builder.CreateCall(function, orderedArgs, functionName);

    // Get the SymbolType from the function's return type
    SymbolType returnType = getSymbolTypeFromLLVMType(function->getReturnType());

    // Return the result of the function call
    return TypedValue(returnType, call);
}

void ASTVisitor::visit(const FunctionDeclNode &node)
{
    // Get function name
    std::string functionName = node.getName();

    // Get the return type
    std::string returnType = node.getReturnType();

    // Check if the function already exists in the current scope
    if (scopeStack.getCurrentScopeSymbol(functionName))
    {
        std::cerr << "Function " << functionName << " already exists in the current scope\n";
        std::cout << node.getTokenText() << '\n';
        exit(EXIT_FAILURE);
    }

    // Determine the LLVM return type based on the returnType string
    llvm::Type *returnTypeLLVM;
    if (returnType == "int")
    {
        returnTypeLLVM = llvm::Type::getInt32Ty(context);
    }
    else if (returnType == "float")
    {
        returnTypeLLVM = llvm::Type::getFloatTy(context);
    }
    else if (returnType == "bool")
    {
        returnTypeLLVM = llvm::Type::getInt1Ty(context);
    }
    else if (returnType == "str")
    {
        returnTypeLLVM = llvm::Type::getInt8PtrTy(context); // Pointer to array of 8-bit integers
    }
    else if (returnType == "void")
    {
        returnTypeLLVM = llvm::Type::getVoidTy(context);
    }
    else
    {
        std::cerr << "Unknown return type: " << returnType << '\n';
        std::cout << node.getTokenText() << '\n';
        exit(EXIT_FAILURE);
    }

    // Set up function and parameters
    std::vector<llvm::Type *> paramTypes;
    std::map<std::string, TypedValue> params;
    std::map<std::string, int> functionParameterIndices;
    std::map<std::string, SymbolType> functionParameterTypes;

    // Get the parameters
    auto paramsNode = node.getParameters();

    if (paramsNode)
        params = visit(*paramsNode);

    int index = 0;
    for (const auto &param : params)
    {
        std::cout << "Parameter: " << param.first << " Type: " << getTypeStringFromSymbolType(param.second.type) << '\n';

        // Determine the LLVM parameter type based on the paramType string
        llvm::Type *paramTypeLLVM = getLLVMTypeFromSymbolType(param.second.type);

        // Add the parameter type to the vector of parameter types
        paramTypes.push_back(paramTypeLLVM);

        // Add the parameter index and type to the function symbol
        functionParameterIndices[param.first] = index;
        functionParameterTypes[param.first] = param.second.type;

        index++;
    }

    // Declare the function with parameter types
    llvm::FunctionType *functionType = llvm::FunctionType::get(returnTypeLLVM, paramTypes, false);
    llvm::Function *function = llvm::Function::Create(functionType, llvm::Function::ExternalLinkage, functionName, module.get());

    // Set up function symbol
    Symbol functionSymbol;
    functionSymbol.name = functionName;
    functionSymbol.type = SymbolType::Function;
    functionSymbol.value = function;
    functionSymbol.functionParameterIndices = functionParameterIndices;
    functionSymbol.functionParameterTypes = functionParameterTypes;
    functionSymbol.functionReturnType = returnTypeLLVM;
    functionSymbol.isFunctionParameter = false;

    // Store the symbol in the current scope
    scopeStack.addSymbol(functionSymbol);

    // Store the current insertion point before modifying it
    llvm::IRBuilderBase::InsertPoint currentInsertionPoint = builder.saveIP();

    // Create a new basic block for the function body and set the builder's insertion point
    llvm::BasicBlock *basicBlock = llvm::BasicBlock::Create(context, "entry", function);
    builder.SetInsertPoint(basicBlock);

    // Push a new scope for the function
    scopeStack.pushScope();

    // Set up parameter symbols and add them to the function's symbol table
    auto paramIter = params.begin();
    for (auto &arg : function->args())
    {
        if (paramIter != params.end())
        {
            // Get the parameter name and value
            std::string paramName = paramIter->first;
            TypedValue &paramValue = paramIter->second;

            // Set the argument name
            arg.setName(paramName);

            // Set the parameter value to the argument
            paramValue.value = &arg;

            // Create a new symbol for the parameter
            Symbol paramSymbol;
            paramSymbol.name = paramName;
            paramSymbol.type = paramValue.type; // or whatever type you use for function parameters
            paramSymbol.value = &arg;
            paramSymbol.isFunctionParameter = true;

            // Add the parameter symbol to the function's symbol table
            scopeStack.addSymbol(paramSymbol);

            // Move to the next parameter
            ++paramIter;
        }
    }

    if (node.getBlockBody())
        // Visit the function body
        visit(*node.getBlockBody());

    // Pop the function scope
    scopeStack.popScope();

    // At the end of the function, add a default return statement if not already present
    llvm::BasicBlock *lastBlock = &function->back();
    if (!lastBlock->getTerminator())
    {
        builder.SetInsertPoint(lastBlock);
        if (returnTypeLLVM->isVoidTy())
        {
            builder.CreateRetVoid();
        }
        else
        {
            std::cerr << "Error: Non-void function '" << functionName << "' missing a return statement." << std::endl;
            std::cout << node.getTokenText() << '\n';
            exit(EXIT_FAILURE);
        }
    }

    // Restore the previous insertion point
    builder.restoreIP(currentInsertionPoint);
}

TypedValue ASTVisitor::visit(const IdentifierNode &node)
{
    // Get the identifier
    auto identifier = node.getIdentifier();

    // Get the symbol from the current scope
    Symbol *symbol = scopeStack.getCurrentScopeSymbol(identifier);

    if (!symbol)
    {
        std::cerr << "Error: Identifier '" << identifier << "' not found." << std::endl;
        std::cout << node.getTokenText() << '\n';
        exit(EXIT_FAILURE);
    }

    // Return the symbol as a TypedValue
    return TypedValue(symbol->type, symbol->value);
}

void ASTVisitor::visit(const IfStatementNode &node)
{
    llvm::Function *function = builder.GetInsertBlock()->getParent();

    llvm::BasicBlock *mergeBlock = nullptr;
    llvm::BasicBlock *elseBlock = nullptr;

    for (const auto &conditionBlockPair : node.getConditionBlockPairs())
    {
        // Visit the condition expression and get its value
        TypedValue conditionValue = visit(*conditionBlockPair.first);

        // Ensure the condition is a boolean
        if (conditionValue.type != SymbolType::Boolean)
        {
            conditionValue.value = builder.CreateICmpNE(conditionValue.value, llvm::ConstantInt::get(context, llvm::APInt(32, 0, true)), "to_bool");
        }

        // Create blocks for the 'then' and 'else' parts of the if statement
        llvm::BasicBlock *thenBlock = llvm::BasicBlock::Create(context, "then", function);
        llvm::BasicBlock *nextElseBlock = llvm::BasicBlock::Create(context, "else", function);

        // Create the conditional branch instruction
        builder.CreateCondBr(conditionValue.value, thenBlock, nextElseBlock);

        // Generate the 'then' block code
        builder.SetInsertPoint(thenBlock);
        if (conditionBlockPair.second)
        {
            visit(*conditionBlockPair.second);
        }

        // If the 'then' block doesn't have a terminator, add a branch to the 'else' block
        if (!thenBlock->getTerminator())
        {
            builder.CreateBr(nextElseBlock);
        }

        // Start insertion in 'else' block
        builder.SetInsertPoint(nextElseBlock);

        // If there was a previous else block and it doesn't have a terminator, add a branch to the next else block
        if (elseBlock && !elseBlock->getTerminator())
        {
            builder.SetInsertPoint(elseBlock);
            builder.CreateBr(nextElseBlock);
        }

        elseBlock = nextElseBlock;
    }

    // Visit the else block body if it exists
    if (node.getElseBlockBody())
    {
        builder.SetInsertPoint(elseBlock);
        visit(*node.getElseBlockBody());
    }

    // If the last 'else' block doesn't have a terminator, add a branch to the 'merge' block
    if (elseBlock && !elseBlock->getTerminator())
    {
        builder.SetInsertPoint(elseBlock);
        // Create the 'merge' block if it doesn't exist
        if (!mergeBlock)
        {
            mergeBlock = llvm::BasicBlock::Create(context, "merge", function);
        }
        builder.CreateBr(mergeBlock);
    }

    // Start insertion in 'merge' block if it exists
    if (mergeBlock)
    {
        builder.SetInsertPoint(mergeBlock);
    }
}

TypedValue ASTVisitor::visit(const IntLiteralNode &node)
{
    int value = node.getValue();
    return TypedValue(SymbolType::Integer, llvm::ConstantInt::get(context, llvm::APInt(32, value)));
}

TypedValue ASTVisitor::visit(const FloatLiteralNode &node)
{
    float value = node.getValue();
    return TypedValue(SymbolType::Float, llvm::ConstantFP::get(context, llvm::APFloat(value)));
}

TypedValue ASTVisitor::visit(const BooleanLiteralNode &node)
{
    bool value = node.getValue();
    return TypedValue(SymbolType::Boolean, llvm::ConstantInt::get(context, llvm::APInt(1, value)));
}

TypedValue ASTVisitor::visit(const StringLiteralNode &node)
{
    std::string value = node.getValue();

    // Create a global constant array initialized with the string data
    llvm::Constant *strConstant = llvm::ConstantDataArray::getString(context, value);

    // Create a global variable to hold the string data
    llvm::GlobalVariable *strVar = new llvm::GlobalVariable(
        *module,
        strConstant->getType(),
        true, // isConstant
        llvm::GlobalValue::PrivateLinkage,
        strConstant,
        ".str");

    // Get a pointer to the first element of the array
    llvm::Value *strValue = builder.CreateConstGEP2_32(llvm::Type::getInt8Ty(context), strVar, 0, 0);

    return TypedValue(SymbolType::String, strValue);
}

TypedValue ASTVisitor::visit(const MultiplicationNode &node)
{
    // Get the left and right operands
    TypedValue left = visit(*node.getLeftOperand());
    TypedValue right = visit(*node.getRightOperand());

    // Set up left and right llvm types and values
    llvm::Type *leftType = left.value->getType();
    llvm::Type *rightType = right.value->getType();
    llvm::Value *leftValue = left.value;
    llvm::Value *rightValue = right.value;

    // Type promotion for mixed arithmetic
    if (leftType->isIntegerTy() && rightType->isFloatTy())
    {
        leftValue = builder.CreateSIToFP(leftValue, llvm::Type::getFloatTy(context), "int_to_float");
        leftType = llvm::Type::getFloatTy(context);
    }
    else if (leftType->isFloatTy() && rightType->isIntegerTy())
    {
        rightValue = builder.CreateSIToFP(rightValue, llvm::Type::getFloatTy(context), "int_to_float");
        rightType = llvm::Type::getFloatTy(context);
    }

    // Integer arithmetic
    if (leftType->isIntegerTy() && rightType->isIntegerTy())
    {
        if (node.getOperatorType() == MultiplicationOperator::MUL)
        {
            return {SymbolType::Integer, builder.CreateMul(leftValue, rightValue, "mul")};
        }
        else if (node.getOperatorType() == MultiplicationOperator::DIV)
        {
            // Convert both operands to float before division
            leftValue = builder.CreateSIToFP(leftValue, llvm::Type::getFloatTy(context), "int_to_float_left");
            rightValue = builder.CreateSIToFP(rightValue, llvm::Type::getFloatTy(context), "int_to_float_right");
            return {SymbolType::Float, builder.CreateFDiv(leftValue, rightValue, "fdiv")};
        }
    }
    // Floating-point arithmetic
    else if (leftType->isFloatTy() && rightType->isFloatTy())
    {
        if (node.getOperatorType() == MultiplicationOperator::MUL)
        {
            return {SymbolType::Float, builder.CreateFMul(leftValue, rightValue, "fmul")};
        }
        else if (node.getOperatorType() == MultiplicationOperator::DIV)
        {
            return {SymbolType::Float, builder.CreateFDiv(leftValue, rightValue, "fdiv")};
        }
    }

    // If we reach here, it means we have an unimplemented case
    std::cerr << "Error: Unimplemented arithmetics for multiplication node\n";
    std::cout << node.getTokenText() << '\n';
    exit(EXIT_FAILURE);
}

TypedValue ASTVisitor::visit(const UnaryNode &node)
{
    // Get the operand
    TypedValue operand = visit(*node.getOperand());

    // Set up operand llvm type and value
    llvm::Type *operandType = operand.value->getType();
    llvm::Value *operandValue = operand.value;

    // Check the operator type
    if (node.getOperatorType() == UnaryOperator::SUB)
    {
        // For integer types
        if (operandType->isIntegerTy())
        {
            return {SymbolType::Integer, builder.CreateNeg(operandValue, "neg")};
        }
        // For floating-point types
        else if (operandType->isFloatTy())
        {
            return {SymbolType::Float, builder.CreateFNeg(operandValue, "fneg")};
        }
    }

    // If we reach here, it means we have an unimplemented case
    std::cerr << "Error: Unimplemented operation for unary node\n";
    std::cout << node.getTokenText() << '\n';
    exit(EXIT_FAILURE);
}

TypedValue ASTVisitor::visit(const TypeCastNode &node)
{
    // Get the operand expression
    TypedValue operand = visit(*node.getOperand());

    // Get the target type
    SymbolType targetType = getSymbolTypeFromTypeString(node.getType());

    // Get the original type
    SymbolType originalType = operand.type;

    // Throw error is the target type is a void type
    if (targetType == SymbolType::Void || originalType == SymbolType::Void)
    {
        std::cerr << "Error: Cannot cast to or from void type." << std::endl;
        std::cout << node.getTokenText() << '\n';
        exit(EXIT_FAILURE);
    }

    // Check if the value type is the same as the target type
    if (originalType == targetType)
    {
        return operand;
    }

    // Get the value
    llvm::Value *value = operand.value;

    // Set up cast result value
    llvm::Value *castResult;

    if (targetType == SymbolType::Float)
    {
        if (originalType == SymbolType::Boolean)
        {
            castResult = boolToFloat(value);
        }
        else if (originalType == SymbolType::Integer)
        {
            castResult = intToFloat(value);
        }
        else
        {
            std::cerr << "Error: Unimplemented type cast to float: " << getTypeStringFromSymbolType(originalType) << "\n";
            std::cout << node.getTokenText() << '\n';
            exit(EXIT_FAILURE);
        }
    }
    else if (targetType == SymbolType::Boolean)
    {
        if (originalType == SymbolType::Float)
        {
            castResult = floatToBool(value);
        }
        else if (originalType == SymbolType::Integer)
        {
            castResult = intToBool(value);
        }
        else if (originalType == SymbolType::String)
        {
            castResult = stringToBool(value);
        }
        else
        {
            std::cerr << "Error: Unimplemented type cast to boolean: " << getTypeStringFromSymbolType(originalType) << "\n";
            std::cout << node.getTokenText() << '\n';
            exit(EXIT_FAILURE);
        }
    }
    else if (targetType == SymbolType::Integer)
    {
        if (originalType == SymbolType::Float)
        {
            castResult = floatToInt(value);
        }
        else if (originalType == SymbolType::Boolean)
        {
            castResult = boolToInt(value);
        }
        else
        {
            std::cerr << "Error: Unimplemented type cast to integer: " << getTypeStringFromSymbolType(originalType) << "\n";
            std::cout << node.getTokenText() << '\n';
            exit(EXIT_FAILURE);
        }
    }
    else if (targetType == SymbolType::String)
    {
        if (originalType == SymbolType::Integer)
        {
            castResult = intToString(value);
        }
        else if (originalType == SymbolType::Boolean)
        {
            castResult = boolToString(value);
        }
        else if (originalType == SymbolType::Float)
        {
            castResult = floatToString(value);
        }
        else
        {
            std::cerr << "Error: Unimplemented type cast to string: " << getTypeStringFromSymbolType(originalType) << "\n";
            std::cout << node.getTokenText() << '\n';
            exit(EXIT_FAILURE);
        }
    }
    else
    {
        std::cerr << "Error: Unimplemented target type for type cast node: " << getTypeStringFromSymbolType(targetType) << "\n";
        std::cout << node.getTokenText() << '\n';
        exit(EXIT_FAILURE);
    }

    // Return the cast result
    return {targetType, castResult};
}

std::pair<std::string, TypedValue> ASTVisitor::visit(const ParamNode &node)
{
    // Get the parameter name and type
    std::string paramName = node.getName();
    std::string paramType = node.getType();

    // Determine the LLVM parameter type based on the paramType string
    llvm::Type *paramTypeLLVM = getLLVMTypeFromTypeString(paramType);

    // The value of the parameter is null, as it is not initialized yet
    TypedValue paramValue{getSymbolTypeFromTypeString(paramType), nullptr};

    return {paramName, paramValue};
}

std::map<std::string, TypedValue> ASTVisitor::visit(const ParamsNode &node)
{
    std::map<std::string, TypedValue> params;
    for (const auto &paramNode : node.getParams())
    {
        // Visit the ParamNode to get the name and TypedValue
        std::pair<std::string, TypedValue> param = visit(*paramNode);

        // Insert the TypedValue into the map with the parameter name as the key
        params[param.first] = param.second;
    }
    return params;
}

void ASTVisitor::visit(const ReturnStatementNode &node)
{
    std::string functionName = builder.GetInsertBlock()->getParent()->getName().str();

    // Get the current function symbol
    Symbol *currentFunctionSymbol = getSymbol(functionName);

    if (!currentFunctionSymbol)
    {
        std::cerr << "Error: Function '" << functionName << "' not found." << std::endl;
        std::cout << node.getTokenText() << '\n';
        exit(EXIT_FAILURE);
    }

    if (currentFunctionSymbol->functionReturnType->isVoidTy())
    {
        // Throw an error, as the current function should not have a return statement
        std::cerr << "Error: Return statement in a function with no return type." << std::endl;
        std::cout << node.getTokenText() << '\n';
        exit(EXIT_FAILURE);
    }

    TypedValue returnedValue = visit(*node.getReturnExpression());

    if (currentFunctionSymbol->functionReturnType->isIntegerTy(1) && returnedValue.value->getType()->isIntegerTy(1))
    {
        builder.CreateRet(returnedValue.value);
    }
    else if (currentFunctionSymbol->functionReturnType->isIntegerTy(32) && returnedValue.value->getType()->isIntegerTy(32))
    {
        builder.CreateRet(returnedValue.value);
    }
    else if (currentFunctionSymbol->functionReturnType->isFloatTy() && returnedValue.value->getType()->isFloatTy())
    {
        builder.CreateRet(returnedValue.value);
    }
    else if (currentFunctionSymbol->functionReturnType->isPointerTy() && returnedValue.value->getType()->isPointerTy())
    {
        builder.CreateRet(returnedValue.value);
    }
    else
    {
        std::cerr << "Error: Return type mismatch in function." << std::endl;

        // Debug expected return type vs actual return type
        std::cout << "Expected return type: " << getTypeStringFromLLVMType(currentFunctionSymbol->functionReturnType) << '\n';
        std::cout << "Actual return type: " << getTypeStringFromLLVMType(returnedValue.value->getType()) << '\n';

        std::cout << node.getTokenText() << '\n';
        exit(EXIT_FAILURE);
    }
}

void ASTVisitor::visit(const StatementNode &node)
{
    auto statementNode = node.getStatement();
    if (auto assignmentNode = std::dynamic_pointer_cast<AssignmentNode>(statementNode))
    {
        visit(*assignmentNode);
    }
    else if (auto functionDeclNode = std::dynamic_pointer_cast<FunctionDeclNode>(statementNode))
    {
        visit(*functionDeclNode);
    }
    else if (auto functionCallNode = std::dynamic_pointer_cast<FunctionCallNode>(statementNode))
    {
        visit(*functionCallNode);
    }
    else if (auto returnStatementNode = std::dynamic_pointer_cast<ReturnStatementNode>(statementNode))
    {
        visit(*returnStatementNode);
    }
    else if (auto ifStatementNode = std::dynamic_pointer_cast<IfStatementNode>(statementNode))
    {
        visit(*ifStatementNode);
    }
    else
    {
        throw std::runtime_error("Unknown statement type");
    }
}

SymbolType ASTVisitor::getSymbolTypeFromTypeString(const std::string &typeString)
{
    if (typeString == "int")
    {
        return SymbolType::Integer;
    }
    else if (typeString == "float")
    {
        return SymbolType::Float;
    }
    else if (typeString == "bool")
    {
        return SymbolType::Boolean;
    }
    else if (typeString == "str")
    {
        return SymbolType::String;
    }
    else
    {
        std::cerr << "Unknown type: " << typeString << '\n';
        exit(EXIT_FAILURE);
    }
}

llvm::Type *ASTVisitor::getLLVMTypeFromTypeString(const std::string &typeString)
{
    if (typeString == "int")
    {
        return llvm::Type::getInt32Ty(context);
    }
    else if (typeString == "float")
    {
        return llvm::Type::getFloatTy(context);
    }
    else if (typeString == "bool")
    {
        return llvm::Type::getInt1Ty(context);
    }
    else if (typeString == "str")
    {
        return llvm::Type::getInt8PtrTy(context); // Pointer to array of 8-bit integers
    }
    else
    {
        std::cerr << "Unknown type: " << typeString << '\n';
        exit(EXIT_FAILURE);
    }
}

std::string ASTVisitor::getTypeStringFromNodeType(const NodeType &nodeType)
{
    switch (nodeType)
    {
    case NodeType::AdditionNode:
        return "AdditionNode";
    case NodeType::ArgNode:
        return "ArgNode";
    case NodeType::ArgsNode:
        return "ArgsNode";
    case NodeType::AssignmentNode:
        return "AssignmentNode";
    case NodeType::BooleanLiteralNode:
        return "BooleanLiteralNode";
    case NodeType::BlockBodyNode:
        return "BlockBodyNode";
    case NodeType::ComparisonNode:
        return "ComparisonNode";
    case NodeType::EqualityNode:
        return "EqualityNode";
    case NodeType::ExpressionNode:
        return "ExpressionNode";
    case NodeType::FileNode:
        return "FileNode";
    case NodeType::FloatLiteralNode:
        return "FloatLiteralNode";
    case NodeType::FunctionCallNode:
        return "FunctionCallNode";
    case NodeType::FunctionDeclNode:
        return "FunctionDeclNode";
    case NodeType::IdentifierNode:
        return "IdentifierNode";
    case NodeType::IfStatementNode:
        return "IfStatementNode";
    case NodeType::IntLiteralNode:
        return "IntLiteralNode";
    case NodeType::LiteralNode:
        return "LiteralNode";
    case NodeType::MultiplicationNode:
        return "MultiplicationNode";
    case NodeType::ParamNode:
        return "ParamNode";
    case NodeType::ParamsNode:
        return "ParamsNode";
    case NodeType::ReturnStatementNode:
        return "ReturnStatementNode";
    case NodeType::RootNode:
        return "RootNode";
    case NodeType::StatementNode:
        return "StatementNode";
    case NodeType::StringLiteralNode:
        return "StringLiteralNode";
    default:
        return "Unknown";
    }
}

std::string ASTVisitor::getTypeStringFromSymbolType(const SymbolType &symbolType)
{
    switch (symbolType)
    {
    case SymbolType::Integer:
        return "int";
    case SymbolType::Float:
        return "float";
    case SymbolType::Boolean:
        return "bool";
    case SymbolType::String:
        return "str";
    case SymbolType::Function:
        return "function";
    default:
        return "Unknown";
    }
}

std::string ASTVisitor::getTypeStringFromLLVMType(llvm::Type *type)
{
    if (type->isArrayTy())
    {
        return "array";
    }
    else if (type->isIntegerTy())
    {
        if (type->getIntegerBitWidth() == 1)
        {
            return "bool";
        }
        else
        {
            return "int";
        }
    }
    else if (type->isFloatTy())
    {
        return "float";
    }
    else if (type->isVoidTy())
    {
        return "void";
    }
    else if (type->isPointerTy())
    {
        // Assume all pointers are strings until pointers are implemented
        return "str";
    }
    else
    {
        return "Unknown";
    }
}

SymbolType ASTVisitor::getSymbolTypeFromLLVMType(llvm::Type *type)
{
    if (type->isArrayTy())
    {
        return SymbolType::Array;
    }
    else if (type->isIntegerTy())
    {
        return SymbolType::Integer;
    }
    else if (type->isFloatTy())
    {
        return SymbolType::Float;
    }
    else if (type->isVoidTy())
    {
        return SymbolType::Void;
    }
    else if (type->isPointerTy())
    {
        return SymbolType::String;
    }
    else
    {
        std::cerr << "Unknown type: " << getTypeStringFromLLVMType(type) << '\n';
        exit(EXIT_FAILURE);
    }
}

llvm::Type *ASTVisitor::getLLVMTypeFromSymbolType(const SymbolType &symbolType, const SymbolType &elementType = SymbolType::Void, uint64_t arraySize = 0)
{
    if (symbolType == SymbolType::Integer)
    {
        return llvm::Type::getInt32Ty(context);
    }
    else if (symbolType == SymbolType::Float)
    {
        return llvm::Type::getFloatTy(context);
    }
    else if (symbolType == SymbolType::Boolean)
    {
        return llvm::Type::getInt1Ty(context);
    }
    else if (symbolType == SymbolType::Void)
    {
        return llvm::Type::getVoidTy(context);
    }
    else if (symbolType == SymbolType::Array)
    {
        llvm::Type *elementTypeLLVM = getLLVMTypeFromSymbolType(elementType);
        return llvm::ArrayType::get(elementTypeLLVM, arraySize);
    }
    else
    {
        std::cerr << "Unknown type: " << getTypeStringFromSymbolType(symbolType) << '\n';
        exit(EXIT_FAILURE);
    }
}

llvm::Type *ASTVisitor::getLLVMTypeFromNodeType(const NodeType &nodeType)
{
    switch (nodeType)
    {
    case NodeType::IntLiteralNode:
        return llvm::Type::getInt32Ty(context);
    case NodeType::FloatLiteralNode:
        return llvm::Type::getFloatTy(context);
    case NodeType::BooleanLiteralNode:
        return llvm::Type::getInt1Ty(context);
    case NodeType::StringLiteralNode:
        return llvm::Type::getInt8PtrTy(context);
    default:
        throw std::runtime_error("Unknown node type");
    }
}

llvm::Value *ASTVisitor::intToString(llvm::Value *intValue)
{
    // Get the sprintf function
    llvm::Function *sprintfFunc = getSprintfFunc();

    // Allocate a buffer to hold the string
    llvm::Value *strPtr = builder.CreateAlloca(builder.getInt8Ty(), builder.getInt32(33)); // 32 digits + 1 for the null terminator

    // Call the sprintf function
    builder.CreateCall(sprintfFunc, {strPtr, builder.CreateGlobalStringPtr("%d"), intValue});

    return strPtr;
}

llvm::Value *ASTVisitor::floatToString(llvm::Value *value)
{
    // Get the sprintf function
    llvm::Function *sprintfFunc = getSprintfFunc();

    // Allocate a buffer to hold the string
    llvm::Value *strPtr = builder.CreateAlloca(builder.getInt8Ty(), builder.getInt32(33)); // 32 digits + 1 for the null terminator

    // Call the sprintf function
    builder.CreateCall(sprintfFunc, {strPtr, builder.CreateGlobalStringPtr("%f"), value});

    // TODO: Remove trailing zeros

    return strPtr;
}

llvm::Value *ASTVisitor::boolToString(llvm::Value *value)
{
    // Check if the boolean is true or false
    llvm::ConstantInt *zero = llvm::ConstantInt::get(context, llvm::APInt(1, 0));
    llvm::Value *cmp = builder.CreateICmpEQ(value, zero, "eq");

    // Return "true" if the boolean is true, "false" otherwise
    // Note: This function currently creates a new string for each boolean value
    return builder.CreateSelect(cmp, builder.CreateGlobalStringPtr("false"), builder.CreateGlobalStringPtr("true"));
}

llvm::Value *ASTVisitor::stringToInt(llvm::Value *value)
{
    // TODO: Implement string to int
    std::cerr << "Error: Unimplemented string to int conversion\n";
    exit(EXIT_FAILURE);
}

llvm::Value *ASTVisitor::stringToFloat(llvm::Value *value)
{
    // TODO: Implement string to float
    std::cerr << "Error: Unimplemented string to float conversion\n";
    exit(EXIT_FAILURE);
}

llvm::Value *ASTVisitor::stringToBool(llvm::Value *value)
{
    // Get strlen function
    llvm::Function *strlenFunc = getStrlenFunc();

    // Call strlen function
    llvm::Value *strlenResult = builder.CreateCall(strlenFunc, {value}, "strlen");

    // Compare strlen result with 0
    return builder.CreateICmpNE(strlenResult, builder.getInt64(0), "ne");
}

llvm::Value *ASTVisitor::intToFloat(llvm::Value *value)
{
    // Convert the integer to a float
    return builder.CreateSIToFP(value, llvm::Type::getFloatTy(context));
}

llvm::Value *ASTVisitor::floatToInt(llvm::Value *value)
{
    // Convert the float to an integer
    return builder.CreateFPToSI(value, llvm::Type::getInt32Ty(context));
}

llvm::Value *ASTVisitor::intToBool(llvm::Value *value)
{
    // Compare the integer with 0
    llvm::ConstantInt *zero = llvm::ConstantInt::get(context, llvm::APInt(32, 0));
    // Return true if the integer is not equal to 0
    return builder.CreateICmpNE(value, zero);
}

llvm::Value *ASTVisitor::floatToBool(llvm::Value *value)
{
    // Compare the float with 0.0
    llvm::ConstantFP *zero = llvm::ConstantFP::get(context, llvm::APFloat(0.0));
    // Return true if the float is not equal to 0.0
    return builder.CreateFCmpONE(value, zero);
}

llvm::Value *ASTVisitor::boolToInt(llvm::Value *value)
{
    // Convert the boolean to an integer
    return builder.CreateZExt(value, llvm::Type::getInt32Ty(context));
}

llvm::Value *ASTVisitor::boolToFloat(llvm::Value *value)
{
    // Convert the boolean to a float
    return builder.CreateUIToFP(value, llvm::Type::getFloatTy(context));
}