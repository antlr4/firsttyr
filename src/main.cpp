#include <iostream>
#include <tuple>
#include "FileHandler.h"

const std::tuple<int, int, int> COMPILER_VERSION = std::make_tuple(0, 1, 7);

int main(int argc, const char *argv[])
{
    if (argc > 1 && std::string(argv[1]) == "--test")
    {
        run_tests(COMPILER_VERSION);
    }
    else if (argc > 1)
    {
        std::string filename(argv[1]);
        compile_file(filename);
    }
    else
    {
        compile_file("main.ftyr");
    }

    return 0;
}
