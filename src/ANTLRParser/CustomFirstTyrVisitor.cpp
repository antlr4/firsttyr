#include <iostream>
#include "CustomFirstTyrVisitor.h"
#include "TypeHelpers.h"

CustomFirstTyrVisitor::CustomFirstTyrVisitor(FirstTyrParser &parser, antlr4::CommonTokenStream &tokens, std::shared_ptr<RootNode> astRoot)
    : parser(parser), tokens(tokens), astRoot(astRoot)
{
}

antlrcpp::Any CustomFirstTyrVisitor::visitFile(FirstTyrParser::FileContext *ctx)
{
    std::string tokenText = getTextIncludingHiddenTokens(ctx, tokens);
    auto fileNode = std::make_shared<FileNode>(ctx, tokenText);

    // Visit the function declaration in the file and add it to the root node
    if (ctx->functionDecl())
    {
        antlrcpp::Any result = visit(ctx->functionDecl());

        if (result.type() == typeid(std::shared_ptr<FunctionDeclNode>))
        {
            auto functionDeclNode = std::any_cast<std::shared_ptr<FunctionDeclNode>>(result);
            fileNode->addFunctionDecl(std::move(functionDeclNode));
        }
        else
        {
            errorHandler.addUnexpectedTypeError(result.type(), "FunctionDeclNode", ctx);
            return nullptr;
        }
    }

    // Return the FileNode
    return fileNode;
}

antlrcpp::Any CustomFirstTyrVisitor::visitFunctionDecl(FirstTyrParser::FunctionDeclContext *ctx)
{
    std::string tokenText = getTextIncludingHiddenTokens(ctx, tokens);
    std::string functionName = ctx->ID()->getText();
    std::string functionType;

    if (ctx->TYPE())
    {
        functionType = ctx->TYPE()->getText();
    }
    else
    {
        functionType = "void";
    }

    auto functionDeclNode = std::make_shared<FunctionDeclNode>(functionName, functionType, ctx, tokenText);

    // Handle parameters
    std::shared_ptr<ParamsNode> paramsNode;
    if (ctx->params())
    {
        antlrcpp::Any result = visit(ctx->params());
        if (result.type() == typeid(std::shared_ptr<ParamsNode>))
        {
            paramsNode = std::any_cast<std::shared_ptr<ParamsNode>>(result);
            functionDeclNode->setParameters(paramsNode);
        }
        else
        {
            errorHandler.addUnexpectedTypeError(result.type(), "ParamsNode", ctx);
            return nullptr;
        }
    }

    // Handle block body
    if (ctx->blockBody())
    {
        antlrcpp::Any result = visit(ctx->blockBody());
        if (result.type() == typeid(std::shared_ptr<BlockBodyNode>))
        {
            auto blockBodyNode = std::any_cast<std::shared_ptr<BlockBodyNode>>(result);
            functionDeclNode->setBlockBody(std::move(blockBodyNode));
        }
        else
        {
            errorHandler.addUnexpectedTypeError(result.type(), "BlockBodyNode", ctx);
            return nullptr;
        }
    }

    // Handle return statement
    if (ctx->returnStatement())
    {
        antlrcpp::Any result = visit(ctx->returnStatement());
        if (result.type() == typeid(std::shared_ptr<ReturnStatementNode>))
        {
            auto returnStatementNode = std::any_cast<std::shared_ptr<ReturnStatementNode>>(result);
            functionDeclNode->setReturnStatement(std::move(returnStatementNode));
        }
        else
        {
            errorHandler.addUnexpectedTypeError(result.type(), "ReturnStatementNode", ctx);
            return nullptr;
        }
    }

    return functionDeclNode;
}

antlrcpp::Any CustomFirstTyrVisitor::visitParams(FirstTyrParser::ParamsContext *ctx)
{
    std::string tokenText = getTextIncludingHiddenTokens(ctx, tokens);
    auto paramsNode = std::make_shared<ParamsNode>(ctx, tokenText);
    for (auto paramCtx : ctx->param())
    {
        antlrcpp::Any result = visit(paramCtx);
        if (result.type() == typeid(std::shared_ptr<ParamNode>))
        {
            auto paramNode = std::any_cast<std::shared_ptr<ParamNode>>(result);
            paramsNode->addParam(paramNode);
        }
        else
        {
            errorHandler.addUnexpectedTypeError(result.type(), "ParamNode", ctx);
            return nullptr;
        }
    }
    return paramsNode;
}

antlrcpp::Any CustomFirstTyrVisitor::visitParam(FirstTyrParser::ParamContext *ctx)
{
    std::string tokenText = getTextIncludingHiddenTokens(ctx, tokens);
    std::string paramName = ctx->ID()->getText();
    std::string paramType = ctx->TYPE()->getText();
    auto paramNode = std::make_shared<ParamNode>(paramName, paramType, ctx, tokenText);
    return paramNode;
}

antlrcpp::Any CustomFirstTyrVisitor::visitBlockBody(FirstTyrParser::BlockBodyContext *ctx)
{
    std::string tokenText = getTextIncludingHiddenTokens(ctx, tokens);
    auto blockBodyNode = std::make_shared<BlockBodyNode>(ctx, tokenText);
    for (auto statementContext : ctx->statement())
    {
        antlrcpp::Any result = visit(statementContext);
        if (result.type() == typeid(std::shared_ptr<StatementNode>))
        {
            auto statementNode = std::any_cast<std::shared_ptr<StatementNode>>(result);
            blockBodyNode->addStatement(statementNode);
        }
        else
        {
            errorHandler.addUnexpectedTypeError(result.type(), "StatementNode", ctx);
            return nullptr;
        }
    }
    return blockBodyNode;
}

antlrcpp::Any CustomFirstTyrVisitor::visitStatement(FirstTyrParser::StatementContext *ctx)
{
    std::string tokenText = getTextIncludingHiddenTokens(ctx, tokens);
    if (ctx->assignment())
    {
        antlrcpp::Any result = visit(ctx->assignment());
        if (result.type() == typeid(std::shared_ptr<AssignmentNode>))
        {
            auto node = std::any_cast<std::shared_ptr<AssignmentNode>>(result);
            return std::make_shared<StatementNode>(node, ctx, tokenText);
        }
        else
        {
            errorHandler.addUnexpectedTypeError(result.type(), "AssignmentNode", ctx);
            return nullptr;
        }
    }
    else if (ctx->functionDecl())
    {
        antlrcpp::Any result = visit(ctx->functionDecl());
        if (result.type() == typeid(std::shared_ptr<FunctionDeclNode>))
        {
            auto node = std::any_cast<std::shared_ptr<FunctionDeclNode>>(result);
            return std::make_shared<StatementNode>(node, ctx, tokenText);
        }
        else
        {
            errorHandler.addUnexpectedTypeError(result.type(), "FunctionDeclNode", ctx);
            return nullptr;
        }
    }
    else if (ctx->functionCall())
    {
        antlrcpp::Any result = visit(ctx->functionCall());

        if (result.type() == typeid(std::shared_ptr<FunctionCallNode>))
        {
            auto node = std::any_cast<std::shared_ptr<FunctionCallNode>>(result);
            return std::make_shared<StatementNode>(node, ctx, tokenText);
        }
        else
        {
            errorHandler.addUnexpectedTypeError(result.type(), "FunctionCallNode", ctx);
            return nullptr;
        }
    }
    else if (ctx->returnStatement())
    {
        antlrcpp::Any result = visit(ctx->returnStatement());

        if (result.type() == typeid(std::shared_ptr<ReturnStatementNode>))
        {
            auto node = std::any_cast<std::shared_ptr<ReturnStatementNode>>(result);
            return std::make_shared<StatementNode>(node, ctx, tokenText);
        }
        else
        {
            errorHandler.addUnexpectedTypeError(result.type(), "ReturnStatementNode", ctx);
            return nullptr;
        }
    }
    else if (ctx->ifStatement())
    {
        antlrcpp::Any result = visit(ctx->ifStatement());

        if (result.type() == typeid(std::shared_ptr<IfStatementNode>))
        {
            auto node = std::any_cast<std::shared_ptr<IfStatementNode>>(result);
            return std::make_shared<StatementNode>(node, ctx, tokenText);
        }
        else
        {
            errorHandler.addUnexpectedTypeError(result.type(), "IfStatementNode", ctx);
            return nullptr;
        }
    }
    errorHandler.addInvalidContextError("StatementContext does not contain valid statement", ctx);
    return nullptr;
}

antlrcpp::Any CustomFirstTyrVisitor::visitAssignment(FirstTyrParser::AssignmentContext *ctx)
{
    std::string tokenText = getTextIncludingHiddenTokens(ctx, tokens);
    AssignmentType type = ctx->MUTABLE() ? AssignmentType::Mutable : AssignmentType::Immutable;
    std::string identifier = ctx->ID()->getText();
    std::shared_ptr<ASTNode> valueNode;

    if (ctx->identifier())
    {
        antlrcpp::Any result = visit(ctx->identifier());
        if (result.type() == typeid(std::shared_ptr<IdentifierNode>))
        {
            valueNode = std::any_cast<std::shared_ptr<IdentifierNode>>(result);
        }
        else
        {
            errorHandler.addUnexpectedTypeError(result.type(), "IdentifierNode", ctx);
            return nullptr;
        }
    }
    else if (ctx->expression())
    {
        antlrcpp::Any result = visit(ctx->expression());
        if (result.type() == typeid(std::shared_ptr<ExpressionNode>))
        {
            valueNode = std::any_cast<std::shared_ptr<ExpressionNode>>(result);
        }
        else
        {
            errorHandler.addUnexpectedTypeError(result.type(), "ExpressionNode", ctx);
            return nullptr;
        }
    }
    else
    {
        errorHandler.addInvalidContextError("AssignmentContext does not contain valid identifier or expression", ctx);
        return nullptr;
    }

    return std::make_shared<AssignmentNode>(type, identifier, valueNode, ctx, tokenText);
}

antlrcpp::Any CustomFirstTyrVisitor::visitReturnStatement(FirstTyrParser::ReturnStatementContext *ctx)
{
    std::string tokenText = getTextIncludingHiddenTokens(ctx, tokens);
    if (ctx->expression())
    {
        antlrcpp::Any result = visit(ctx->expression());
        if (result.type() == typeid(std::shared_ptr<ExpressionNode>))
        {
            // Handle node
            auto expressionNode = std::any_cast<std::shared_ptr<ExpressionNode>>(result);
            return std::make_shared<ReturnStatementNode>(expressionNode, ctx, tokenText);
        }
        else
        {
            errorHandler.addUnexpectedTypeError(result.type(), "ExpressionNode", ctx);
            return nullptr;
        }
    }
    errorHandler.addInvalidContextError("ReturnStatementContext does not contain valid expression", ctx);
    return nullptr;
}

antlrcpp::Any CustomFirstTyrVisitor::visitIfStatement(FirstTyrParser::IfStatementContext *ctx)
{
    std::string tokenText = getTextIncludingHiddenTokens(ctx, tokens);
    std::vector<std::pair<std::shared_ptr<ExpressionNode>, std::shared_ptr<BlockBodyNode>>> conditionBlockPairs;

    // Visit the initial if part
    if (ctx->expression())
    {
        antlrcpp::Any result = visit(ctx->expression());
        if (result.type() == typeid(std::shared_ptr<ExpressionNode>))
        {
            auto conditionNode = std::any_cast<std::shared_ptr<ExpressionNode>>(result);
            std::shared_ptr<BlockBodyNode> blockBodyNode = nullptr;
            if (ctx->blockBody())
            {
                antlrcpp::Any blockBodyResult = visit(ctx->blockBody());
                blockBodyNode = std::any_cast<std::shared_ptr<BlockBodyNode>>(blockBodyResult);
            }
            conditionBlockPairs.push_back({conditionNode, blockBodyNode});
        }
        else
        {
            errorHandler.addUnexpectedTypeError(result.type(), "ExpressionNode", ctx);
            return nullptr;
        }
    }

    // Visit the else if parts
    for (auto elseIfPart : ctx->elseIfPart())
    {
        antlrcpp::Any result = visit(elseIfPart);
        if (result.type() == typeid(std::pair<std::shared_ptr<ExpressionNode>, std::shared_ptr<BlockBodyNode>>))
        {
            auto conditionBlockPair = std::any_cast<std::pair<std::shared_ptr<ExpressionNode>, std::shared_ptr<BlockBodyNode>>>(result);
            conditionBlockPairs.push_back(conditionBlockPair);
        }
        else
        {
            errorHandler.addUnexpectedTypeError(result.type(), "pair<ExpressionNode, BlockBodyNode>", ctx);
            return nullptr;
        }
    }

    // Visit the else part
    std::shared_ptr<BlockBodyNode> elseBlockBody = nullptr;
    if (ctx->elsePart())
    {
        antlrcpp::Any result = visit(ctx->elsePart());
        if (result.type() == typeid(std::shared_ptr<BlockBodyNode>))
        {
            elseBlockBody = std::any_cast<std::shared_ptr<BlockBodyNode>>(result);
        }
        else
        {
            errorHandler.addUnexpectedTypeError(result.type(), "BlockBodyNode", ctx);
            return nullptr;
        }
    }

    return std::make_shared<IfStatementNode>(conditionBlockPairs, elseBlockBody, ctx, tokenText);
}

antlrcpp::Any CustomFirstTyrVisitor::visitElseIfPart(FirstTyrParser::ElseIfPartContext *ctx)
{
    std::string tokenText = getTextIncludingHiddenTokens(ctx, tokens);
    std::shared_ptr<ExpressionNode> conditionNode = nullptr;
    std::shared_ptr<BlockBodyNode> blockBodyNode = nullptr;

    // Visit the expression
    if (ctx->expression())
    {
        antlrcpp::Any result = visit(ctx->expression());
        if (result.type() == typeid(std::shared_ptr<ExpressionNode>))
        {
            conditionNode = std::any_cast<std::shared_ptr<ExpressionNode>>(result);
        }
        else
        {
            errorHandler.addUnexpectedTypeError(result.type(), "ExpressionNode", ctx);
            return nullptr;
        }
    }

    // Visit the block body
    if (ctx->blockBody())
    {
        antlrcpp::Any result = visit(ctx->blockBody());
        if (result.type() == typeid(std::shared_ptr<BlockBodyNode>))
        {
            blockBodyNode = std::any_cast<std::shared_ptr<BlockBodyNode>>(result);
        }
        else
        {
            errorHandler.addUnexpectedTypeError(result.type(), "BlockBodyNode", ctx);
            return nullptr;
        }
    }

    return std::make_pair(conditionNode, blockBodyNode);
}

antlrcpp::Any CustomFirstTyrVisitor::visitElsePart(FirstTyrParser::ElsePartContext *ctx)
{
    std::string tokenText = getTextIncludingHiddenTokens(ctx, tokens);
    std::shared_ptr<BlockBodyNode> blockBodyNode = nullptr;

    // Visit the block body
    if (ctx->blockBody())
    {
        antlrcpp::Any result = visit(ctx->blockBody());
        if (result.type() == typeid(std::shared_ptr<BlockBodyNode>))
        {
            blockBodyNode = std::any_cast<std::shared_ptr<BlockBodyNode>>(result);
        }
        else
        {
            errorHandler.addUnexpectedTypeError(result.type(), "BlockBodyNode", ctx);
            return nullptr;
        }
    }

    return blockBodyNode;
}

antlrcpp::Any CustomFirstTyrVisitor::visitExpression(FirstTyrParser::ExpressionContext *ctx)
{
    std::string tokenText = getTextIncludingHiddenTokens(ctx, tokens);
    if (ctx->expression())
    {
        EqualityOperator op;
        if (ctx->EQ())
        {
            op = EqualityOperator::EQ;
        }
        else if (ctx->NE())
        {
            op = EqualityOperator::NE;
        }
        else
        {
            errorHandler.addSyntaxError("Invalid operator in expression context", ctx);
            return nullptr;
        }
        antlrcpp::Any leftResult = visit(ctx->expression());
        if (leftResult.type() == typeid(std::shared_ptr<ExpressionNode>))
        {
            auto leftNode = std::any_cast<std::shared_ptr<ExpressionNode>>(leftResult);
            antlrcpp::Any rightResult = visit(ctx->comparison());
            if (rightResult.type() == typeid(std::shared_ptr<ExpressionNode>))
            {
                auto rightNode = std::any_cast<std::shared_ptr<ExpressionNode>>(rightResult);
                auto equalityNode = std::make_shared<EqualityNode>(leftNode, op, rightNode, ctx, tokenText);
                return std::make_shared<ExpressionNode>(equalityNode, ctx, tokenText);
            }
            else
            {
                errorHandler.addUnexpectedTypeError(rightResult.type(), "ExpressionNode", ctx);
                return nullptr;
            }
        }
        else
        {
            errorHandler.addUnexpectedTypeError(leftResult.type(), "ExpressionNode", ctx);
            return nullptr;
        }
    }
    else if (ctx->comparison())
    {
        return visit(ctx->comparison());
    }
    errorHandler.addInvalidContextError("ExpressionContext does not contain valid expression or comparison", ctx);
    return nullptr;
}

antlrcpp::Any CustomFirstTyrVisitor::visitComparison(FirstTyrParser::ComparisonContext *ctx)
{
    std::string tokenText = getTextIncludingHiddenTokens(ctx, tokens);
    if (ctx->comparison())
    {
        ComparisonOperator op;
        if (ctx->LT())
        {
            op = ComparisonOperator::LT;
        }
        else if (ctx->GT())
        {
            op = ComparisonOperator::GT;
        }
        else if (ctx->LE())
        {
            op = ComparisonOperator::LE;
        }
        else if (ctx->GE())
        {
            op = ComparisonOperator::GE;
        }
        else
        {
            errorHandler.addSyntaxError("Invalid operator in comparison context", ctx);
            return nullptr;
        }
        antlrcpp::Any leftResult = visit(ctx->comparison());
        if (leftResult.type() == typeid(std::shared_ptr<ExpressionNode>))
        {
            auto leftNode = std::any_cast<std::shared_ptr<ExpressionNode>>(leftResult);
            antlrcpp::Any rightResult = visit(ctx->addition());
            if (rightResult.type() == typeid(std::shared_ptr<ExpressionNode>))
            {
                auto rightNode = std::any_cast<std::shared_ptr<ExpressionNode>>(rightResult);
                auto comparisonNode = std::make_shared<ComparisonNode>(leftNode, op, rightNode, ctx, tokenText);
                return std::make_shared<ExpressionNode>(comparisonNode, ctx, tokenText);
            }
            else
            {
                errorHandler.addUnexpectedTypeError(rightResult.type(), "ExpressionNode", ctx);
                return nullptr;
            }
        }
        else
        {
            errorHandler.addUnexpectedTypeError(leftResult.type(), "ExpressionNode", ctx);
            return nullptr;
        }
    }
    else if (ctx->addition())
    {
        return visit(ctx->addition());
    }
    errorHandler.addInvalidContextError("ComparisonContext does not contain valid comparison or addition", ctx);
    return nullptr;
}

antlrcpp::Any CustomFirstTyrVisitor::visitAddition(FirstTyrParser::AdditionContext *ctx)
{
    std::string tokenText = getTextIncludingHiddenTokens(ctx, tokens);
    if (ctx->addition())
    {
        AdditionOperator op;
        if (ctx->ADD())
        {
            op = AdditionOperator::ADD;
        }
        else if (ctx->SUB())
        {
            op = AdditionOperator::SUB;
        }
        else
        {
            errorHandler.addSyntaxError("Invalid operator in addition context", ctx);
            return nullptr;
        }

        antlrcpp::Any leftResult = visit(ctx->addition());
        if (leftResult.type() == typeid(std::shared_ptr<ExpressionNode>))
        {
            auto leftNode = std::any_cast<std::shared_ptr<ExpressionNode>>(leftResult);
            antlrcpp::Any rightResult = visit(ctx->multiplication());
            if (rightResult.type() == typeid(std::shared_ptr<ExpressionNode>))
            {
                auto rightNode = std::any_cast<std::shared_ptr<ExpressionNode>>(rightResult);
                auto additionNode = std::make_shared<AdditionNode>(leftNode, op, rightNode, ctx, tokenText);
                return std::make_shared<ExpressionNode>(additionNode, ctx, tokenText);
            }
            else
            {
                errorHandler.addUnexpectedTypeError(rightResult.type(), "ExpressionNode", ctx);
                return nullptr;
            }
        }
        else
        {
            errorHandler.addUnexpectedTypeError(leftResult.type(), "ExpressionNode", ctx);
            return nullptr;
        }
    }
    else if (ctx->multiplication())
    {
        return visit(ctx->multiplication());
    }
    errorHandler.addInvalidContextError("AdditionContext does not contain valid addition or multiplication", ctx);
    return nullptr;
}

antlrcpp::Any CustomFirstTyrVisitor::visitMultiplication(FirstTyrParser::MultiplicationContext *ctx)
{
    std::string tokenText = getTextIncludingHiddenTokens(ctx, tokens);
    if (ctx->multiplication())
    {
        MultiplicationOperator op;
        if (ctx->MUL())
        {
            op = MultiplicationOperator::MUL;
        }
        else if (ctx->DIV())
        {
            op = MultiplicationOperator::DIV;
        }
        else
        {
            errorHandler.addSyntaxError("Invalid operator in multiplication context", ctx);
            return nullptr;
        }

        antlrcpp::Any leftResult = visit(ctx->multiplication());
        if (leftResult.type() == typeid(std::shared_ptr<ExpressionNode>))
        {
            auto leftNode = std::any_cast<std::shared_ptr<ExpressionNode>>(leftResult);
            antlrcpp::Any rightResult = visit(ctx->unary());
            if (rightResult.type() == typeid(std::shared_ptr<ExpressionNode>))
            {
                auto rightNode = std::any_cast<std::shared_ptr<ExpressionNode>>(rightResult);
                auto multiplicationNode = std::make_shared<MultiplicationNode>(leftNode, op, rightNode, ctx, tokenText);
                return std::make_shared<ExpressionNode>(multiplicationNode, ctx, tokenText);
            }
            else
            {
                errorHandler.addUnexpectedTypeError(rightResult.type(), "ExpressionNode", ctx);
                return nullptr;
            }
        }
        else
        {
            errorHandler.addUnexpectedTypeError(leftResult.type(), "ExpressionNode", ctx);
            return nullptr;
        }
    }
    else if (ctx->unary())
    {
        return visit(ctx->unary());
    }
    errorHandler.addInvalidContextError("MultiplicationContext does not contain valid multiplication or unary", ctx);
    return nullptr;
}

antlrcpp::Any CustomFirstTyrVisitor::visitUnary(FirstTyrParser::UnaryContext *ctx)
{
    std::string tokenText = getTextIncludingHiddenTokens(ctx, tokens);
    if (ctx->unary())
    {
        if (ctx->SUB())
        {
            // Check if the operator is used as a suffix
            if (ctx->SUB()->getSymbol()->getTokenIndex() > ctx->unary()->getStart()->getTokenIndex())
            {
                errorHandler.addSyntaxError("'-' can only be used as a prefix operator.", ctx);
                return nullptr;
            }

            antlrcpp::Any result = visit(ctx->unary());
            if (result.type() == typeid(std::shared_ptr<ExpressionNode>))
            {
                auto expressionNode = std::any_cast<std::shared_ptr<ExpressionNode>>(result);
                auto unaryNode = std::make_shared<UnaryNode>(expressionNode, UnaryOperator::SUB, true, ctx, tokenText);
                return std::make_shared<ExpressionNode>(unaryNode, ctx, tokenText);
            }
            else
            {
                errorHandler.addUnexpectedTypeError(result.type(), "ExpressionNode", ctx);
                return nullptr;
            }
        }
        else
        {
            errorHandler.addSyntaxError("Invalid operator in unary context", ctx);
            return nullptr;
        }
    }
    else if (ctx->typeCast())
    {
        return visit(ctx->typeCast());
    }
    errorHandler.addInvalidContextError("UnaryContext does not contain valid unary or typeCast", ctx);
    return nullptr;
}

antlrcpp::Any CustomFirstTyrVisitor::visitTypeCast(FirstTyrParser::TypeCastContext *ctx)
{
    std::string tokenText = getTextIncludingHiddenTokens(ctx, tokens);
    if (ctx->typeCast())
    {
        std::string type = ctx->TYPE()->getText();
        antlrcpp::Any result = visit(ctx->typeCast());
        if (result.type() == typeid(std::shared_ptr<ExpressionNode>))
        {
            auto expressionNode = std::any_cast<std::shared_ptr<ExpressionNode>>(result);
            auto typeCastNode = std::make_shared<TypeCastNode>(expressionNode, type, ctx, tokenText);
            return std::make_shared<ExpressionNode>(typeCastNode, ctx, tokenText);
        }
        else
        {
            errorHandler.addUnexpectedTypeError(result.type(), "ExpressionNode", ctx);
            return nullptr;
        }
    }
    else if (ctx->atom())
    {
        return visit(ctx->atom());
    }
    errorHandler.addInvalidContextError("TypeCastContext does not contain valid typeCast or atom", ctx);
    return nullptr;
}

antlrcpp::Any CustomFirstTyrVisitor::visitAtom(FirstTyrParser::AtomContext *ctx)
{
    std::string tokenText = getTextIncludingHiddenTokens(ctx, tokens);
    if (ctx->expression())
    {
        antlrcpp::Any result = visit(ctx->expression());
        if (result.type() == typeid(std::shared_ptr<ExpressionNode>))
        {
            return result;
        }
        else
        {
            errorHandler.addUnexpectedTypeError(result.type(), "ExpressionNode", ctx);
            return nullptr;
        }
    }
    else if (ctx->functionCall())
    {
        antlrcpp::Any result = visit(ctx->functionCall());
        if (result.type() == typeid(std::shared_ptr<FunctionCallNode>))
        {
            auto functionCallNode = std::any_cast<std::shared_ptr<FunctionCallNode>>(result);
            return std::make_shared<ExpressionNode>(functionCallNode, ctx, tokenText);
        }
        else
        {
            errorHandler.addUnexpectedTypeError(result.type(), "FunctionCallNode", ctx);
            return nullptr;
        }
    }
    else if (ctx->arrayDeclaration())
    {
        antlrcpp::Any result = visit(ctx->arrayDeclaration());
        if (result.type() == typeid(std::shared_ptr<ArrayNode>))
        {
            auto arrayNode = std::any_cast<std::shared_ptr<ArrayNode>>(result);
            return std::make_shared<ExpressionNode>(arrayNode, ctx, tokenText);
        }
        else
        {
            errorHandler.addUnexpectedTypeError(result.type(), "ArrayNode", ctx);
            return nullptr;
        }
    }
    else if (ctx->arrayIndexing())
    {
        antlrcpp::Any result = visit(ctx->arrayIndexing());
        if (result.type() == typeid(std::shared_ptr<ArrayIndexingNode>))
        {
            auto arrayIndexingNode = std::any_cast<std::shared_ptr<ArrayIndexingNode>>(result);
            return std::make_shared<ExpressionNode>(arrayIndexingNode, ctx, tokenText);
        }
        else
        {
            errorHandler.addUnexpectedTypeError(result.type(), "ArrayIndexingNode", ctx);
            return nullptr;
        }
    }
    else if (ctx->identifier())
    {
        antlrcpp::Any result = visit(ctx->identifier());
        if (result.type() == typeid(std::shared_ptr<IdentifierNode>))
        {
            auto identifierNode = std::any_cast<std::shared_ptr<IdentifierNode>>(result);
            return std::make_shared<ExpressionNode>(identifierNode, ctx, tokenText);
        }
        else
        {
            errorHandler.addUnexpectedTypeError(result.type(), "IdentifierNode", ctx);
            return nullptr;
        }
    }
    else if (ctx->literal())
    {
        antlrcpp::Any result = visit(ctx->literal());
        if (result.type() == typeid(std::shared_ptr<IntLiteralNode>))
        {
            auto literalNode = std::any_cast<std::shared_ptr<IntLiteralNode>>(result);
            return std::make_shared<ExpressionNode>(literalNode, ctx, tokenText);
        }
        else if (result.type() == typeid(std::shared_ptr<FloatLiteralNode>))
        {
            auto literalNode = std::any_cast<std::shared_ptr<FloatLiteralNode>>(result);
            return std::make_shared<ExpressionNode>(literalNode, ctx, tokenText);
        }
        else if (result.type() == typeid(std::shared_ptr<BooleanLiteralNode>))
        {
            auto literalNode = std::any_cast<std::shared_ptr<BooleanLiteralNode>>(result);
            return std::make_shared<ExpressionNode>(literalNode, ctx, tokenText);
        }
        else if (result.type() == typeid(std::shared_ptr<StringLiteralNode>))
        {
            auto literalNode = std::any_cast<std::shared_ptr<StringLiteralNode>>(result);
            return std::make_shared<ExpressionNode>(literalNode, ctx, tokenText);
        }
        else
        {
            errorHandler.addUnexpectedTypeError(result.type(), "IntLiteralNode, FloatLiteralNode, BooleanLiteralNode, or StringLiteralNode", ctx);
            return nullptr;
        }
    }
    errorHandler.addInvalidContextError("AtomContext does not contain valid functionCall, identifier, or literal", ctx);
    return nullptr;
}

antlrcpp::Any CustomFirstTyrVisitor::visitArrayIndexing(FirstTyrParser::ArrayIndexingContext *ctx)
{
    std::string tokenText = getTextIncludingHiddenTokens(ctx, tokens);
    // Visit the identifier node
    antlrcpp::Any idResult = visit(ctx->identifier());
    if (idResult.type() != typeid(std::shared_ptr<IdentifierNode>))
    {
        errorHandler.addUnexpectedTypeError(idResult.type(), "IdentifierNode", ctx);
        return nullptr;
    }
    std::shared_ptr<IdentifierNode> idNode = std::any_cast<std::shared_ptr<IdentifierNode>>(idResult);

    // Visit the expression node
    antlrcpp::Any exprResult = visit(ctx->expression());
    if (exprResult.type() != typeid(std::shared_ptr<ExpressionNode>))
    {
        errorHandler.addUnexpectedTypeError(exprResult.type(), "ExpressionNode", ctx);
        return nullptr;
    }
    std::shared_ptr<ExpressionNode> exprNode = std::any_cast<std::shared_ptr<ExpressionNode>>(exprResult);

    // Create an ArrayIndexingNode with the identifier and expression nodes and return it
    return std::make_shared<ArrayIndexingNode>(ctx, tokenText, idNode, exprNode);
}

antlrcpp::Any CustomFirstTyrVisitor::visitArrayDeclaration(FirstTyrParser::ArrayDeclarationContext *ctx)
{
    std::vector<std::shared_ptr<ExpressionNode>> elements;
    std::string tokenText = getTextIncludingHiddenTokens(ctx, tokens);

    // Visit each expression in the array declaration
    for (auto exprCtx : ctx->expression())
    {
        antlrcpp::Any result = visit(exprCtx);
        if (result.type() == typeid(std::shared_ptr<ExpressionNode>))
        {
            std::shared_ptr<ExpressionNode> element = std::any_cast<std::shared_ptr<ExpressionNode>>(result);
            // This makes the elements in the array ExpressionNode type, not the literal type.
            elements.push_back(element);
        }
        else
        {
            errorHandler.addUnexpectedTypeError(result.type(), "ExpressionNode", ctx);
            return nullptr;
        }
    }

    // Create an ArrayNode with the elements and return it
    return std::make_shared<ArrayNode>(ctx, tokenText, elements);
}

antlrcpp::Any CustomFirstTyrVisitor::visitLiteral(FirstTyrParser::LiteralContext *ctx)
{
    std::string tokenText = getTextIncludingHiddenTokens(ctx, tokens);
    if (ctx->INT())
    {
        int value = std::stoi(ctx->INT()->getText());
        return std::make_shared<IntLiteralNode>(value, ctx, tokenText);
    }
    else if (ctx->FLOAT())
    {
        float value = std::stof(ctx->FLOAT()->getText());
        return std::make_shared<FloatLiteralNode>(value, ctx, tokenText);
    }
    else if (ctx->BOOLEAN())
    {
        if (ctx->BOOLEAN()->getText() != "true" && ctx->BOOLEAN()->getText() != "false")
        {
            errorHandler.addSyntaxError("Boolean value must be either 'true' or 'false'", ctx);
            return nullptr;
        }
        bool value = ctx->BOOLEAN()->getText() == "true";
        return std::make_shared<BooleanLiteralNode>(value, ctx, tokenText);
    }
    else if (ctx->STRING())
    {
        std::string value = ctx->STRING()->getText();
        value = value.substr(1, value.size() - 2); // Remove the first and last characters (the quotes)

        // Replace escape sequences with their corresponding characters
        std::map<std::string, std::string> escapeSequences{
            {"\\n", "\n"},
            {"\\t", "\t"},
            {"\\r", "\r"},
            {"\\\\", "\\"},
            {"\\\"", "\""},
            {"\\'", "'"}};

        for (const auto &seq : escapeSequences)
        {
            size_t pos = 0;
            // Replace all occurrences of the escape sequence with the corresponding character
            while ((pos = value.find(seq.first, pos)) != std::string::npos)
            {
                value.replace(pos, seq.first.length(), seq.second);
                pos += seq.second.length();
            }
        }

        // Handle Unicode escape sequences
        std::regex unicodeEscape("\\\\u\\{([0-9a-fA-F]+)\\}");
        std::smatch match;
        while (std::regex_search(value, match, unicodeEscape))
        {
            std::string unicodeSeq = match[0];
            std::string codePointStr = match[1];
            uint32_t codePoint = std::stoul(codePointStr, nullptr, 16);
            std::string utf8Char = codePointToUtf8(codePoint); // Assuming you have this function
            value.replace(match.position(), unicodeSeq.length(), utf8Char);
        }

        return std::make_shared<StringLiteralNode>(value, ctx, tokenText);
    }
    errorHandler.addInvalidContextError("LiteralContext does not contain valid INT, FLOAT, BOOLEAN or STRING", ctx);
    return nullptr;
}

antlrcpp::Any CustomFirstTyrVisitor::visitArg(FirstTyrParser::ArgContext *ctx)
{
    std::string tokenText = getTextIncludingHiddenTokens(ctx, tokens);
    if (ctx->identifier())
    {
        std::string identifier = ctx->identifier()->ID()->getText();
        antlrcpp::Any result = visit(ctx->identifier());
        if (result.type() == typeid(std::shared_ptr<IdentifierNode>))
        {
            auto identifierNode = std::any_cast<std::shared_ptr<IdentifierNode>>(result);
            auto expressionNode = std::make_shared<ExpressionNode>(identifierNode, ctx, tokenText);
            return std::make_shared<ArgNode>(identifier, expressionNode, ctx, tokenText);
        }
        else
        {
            errorHandler.addUnexpectedTypeError(result.type(), "IdentifierNode", ctx);
            return nullptr;
        }
    }
    else if (ctx->ID() && ctx->expression())
    {
        std::string identifier = ctx->ID()->getText();
        antlrcpp::Any result = visit(ctx->expression());
        if (result.type() == typeid(std::shared_ptr<ExpressionNode>))
        {
            auto expressionNode = std::any_cast<std::shared_ptr<ExpressionNode>>(result);
            return std::make_shared<ArgNode>(identifier, expressionNode, ctx, tokenText);
        }
        else
        {
            errorHandler.addUnexpectedTypeError(result.type(), "ExpressionNode", ctx);
            return nullptr;
        }
    }
    errorHandler.addInvalidContextError("ArgContext does not contain valid arg", ctx);
    return nullptr;
}

antlrcpp::Any CustomFirstTyrVisitor::visitArgs(FirstTyrParser::ArgsContext *ctx)
{
    std::string tokenText = getTextIncludingHiddenTokens(ctx, tokens);
    auto argsNode = std::make_shared<ArgsNode>(ctx, tokenText);
    for (auto argContext : ctx->arg())
    {
        antlrcpp::Any result = visit(argContext);
        if (result.type() == typeid(std::shared_ptr<ArgNode>))
        {
            auto argNode = std::any_cast<std::shared_ptr<ArgNode>>(result);
            argsNode->addArg(argNode);
        }
        else
        {
            errorHandler.addUnexpectedTypeError(result.type(), "ArgNode", ctx);
            return nullptr;
        }
    }
    return argsNode;
}

antlrcpp::Any CustomFirstTyrVisitor::visitFunctionCall(FirstTyrParser::FunctionCallContext *ctx)
{
    std::string tokenText = getTextIncludingHiddenTokens(ctx, tokens);
    std::string functionName = ctx->ID()->getText();

    // If the function name is "print", add the print function to the AST root
    if (functionName == "print")
    {
        astRoot->addStandardFunction("print");
    }

    auto functionCallNode = std::make_shared<FunctionCallNode>(functionName, ctx, tokenText);

    if (ctx->args())
    {
        antlrcpp::Any result = visit(ctx->args());
        if (result.type() == typeid(std::shared_ptr<ArgsNode>))
        {
            auto argsNode = std::any_cast<std::shared_ptr<ArgsNode>>(result);
            functionCallNode->setArguments(argsNode);
        }
        else
        {
            errorHandler.addUnexpectedTypeError(result.type(), "ArgsNode", ctx);
            return nullptr;
        }
    }

    return functionCallNode;
}

antlrcpp::Any CustomFirstTyrVisitor::visitIdentifier(FirstTyrParser::IdentifierContext *ctx)
{
    std::string tokenText = getTextIncludingHiddenTokens(ctx, tokens);
    std::string identifier = ctx->ID()->getText();
    return std::make_shared<IdentifierNode>(identifier, ctx, tokenText);
}
