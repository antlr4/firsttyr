#include "ANTLRHelpers.h"

std::string getTextIncludingHiddenTokens(antlr4::ParserRuleContext *ctx, antlr4::CommonTokenStream &tokens)
{
    std::string result = "";
    for (int i = ctx->start->getTokenIndex(); i <= ctx->stop->getTokenIndex(); ++i)
    {
        antlr4::Token *token = tokens.get(i);
        result += token->getText();
    }
    return result;
}