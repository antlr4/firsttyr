#include "TypeHelpers.h"
#include <iostream>

std::string getTypeName(const std::type_info &typeInfo)
{
    if (typeInfo == typeid(std::shared_ptr<AdditionNode>))
        return "AdditionNode";
    else if (typeInfo == typeid(std::shared_ptr<ArrayNode>))
        return "ArrayNode";
    else if (typeInfo == typeid(std::shared_ptr<ArrayIndexingNode>))
        return "ArrayIndexingNode";
    else if (typeInfo == typeid(std::shared_ptr<ArgNode>))
        return "ArgNode";
    else if (typeInfo == typeid(std::shared_ptr<ArgsNode>))
        return "ArgsNode";
    else if (typeInfo == typeid(std::shared_ptr<ASTNode>))
        return "ASTNode";
    else if (typeInfo == typeid(std::shared_ptr<AssignmentNode>))
        return "AssignmentNode";
    else if (typeInfo == typeid(std::shared_ptr<BlockBodyNode>))
        return "BlockBodyNode";
    else if (typeInfo == typeid(std::shared_ptr<ComparisonNode>))
        return "ComparisonNode";
    else if (typeInfo == typeid(std::shared_ptr<ExpressionNode>))
        return "ExpressionNode";
    else if (typeInfo == typeid(std::shared_ptr<FileNode>))
        return "FileNode";
    else if (typeInfo == typeid(std::shared_ptr<FunctionCallNode>))
        return "FunctionCallNode";
    else if (typeInfo == typeid(std::shared_ptr<FunctionDeclNode>))
        return "FunctionDeclNode";
    else if (typeInfo == typeid(std::shared_ptr<IdentifierNode>))
        return "IdentifierNode";
    else if (typeInfo == typeid(std::shared_ptr<IfStatementNode>))
        return "IfStatementNode";
    else if (typeInfo == typeid(std::shared_ptr<LiteralNode>))
        return "LiteralNode";
    else if (typeInfo == typeid(std::shared_ptr<MultiplicationNode>))
        return "MultiplicationNode";
    else if (typeInfo == typeid(std::shared_ptr<ParamNode>))
        return "ParamNode";
    else if (typeInfo == typeid(std::shared_ptr<ParamsNode>))
        return "ParamsNode";
    else if (typeInfo == typeid(std::shared_ptr<ReturnStatementNode>))
        return "ReturnStatementNode";
    else if (typeInfo == typeid(std::shared_ptr<RootNode>))
        return "RootNode";
    else if (typeInfo == typeid(std::shared_ptr<StatementNode>))
        return "StatementNode";

    return typeInfo.name(); // Fallback to the mangled name if not found
}

std::string codePointToUtf8(uint32_t codePoint)
{
    std::string utf8;
    if (codePoint <= 0x7F)
    {
        utf8.push_back(static_cast<char>(codePoint));
    }
    else if (codePoint <= 0x7FF)
    {
        utf8.push_back(static_cast<char>(0xC0 | ((codePoint >> 6) & 0x1F)));
        utf8.push_back(static_cast<char>(0x80 | (codePoint & 0x3F)));
    }
    else if (codePoint <= 0xFFFF)
    {
        utf8.push_back(static_cast<char>(0xE0 | ((codePoint >> 12) & 0x0F)));
        utf8.push_back(static_cast<char>(0x80 | ((codePoint >> 6) & 0x3F)));
        utf8.push_back(static_cast<char>(0x80 | (codePoint & 0x3F)));
    }
    else
    {
        utf8.push_back(static_cast<char>(0xF0 | ((codePoint >> 18) & 0x07)));
        utf8.push_back(static_cast<char>(0x80 | ((codePoint >> 12) & 0x3F)));
        utf8.push_back(static_cast<char>(0x80 | ((codePoint >> 6) & 0x3F)));
        utf8.push_back(static_cast<char>(0x80 | (codePoint & 0x3F)));
    }
    return utf8;
}