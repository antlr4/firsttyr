find_path(ANTLR_INCLUDE_DIR antlr4-runtime.h PATH_SUFFIXES antlr4-runtime)
find_library(ANTLR_LIBRARY antlr4-runtime)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(ANTLR DEFAULT_MSG ANTLR_INCLUDE_DIR ANTLR_LIBRARY)

if(ANTLR_FOUND)
  set(ANTLR_LIBRARIES ${ANTLR_LIBRARY})
  set(ANTLR_INCLUDE_DIRS ${ANTLR_INCLUDE_DIR})
else()
  set(ANTLR_LIBRARIES)
  set(ANTLR_INCLUDE_DIRS)
endif()

mark_as_advanced(ANTLR_INCLUDE_DIR ANTLR_LIBRARY)
