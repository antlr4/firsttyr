#pragma once
#include <iostream>
#include <fstream>
#include <sstream>
#include <filesystem>
#include <vector>
#include <algorithm>
#include <string>
#include <memory>
#include <tuple>

#include "llvm/Support/TargetSelect.h"
#include "llvm/Support/FileSystem.h"
#include "llvm/Support/Program.h"
#include "llvm/Support/Path.h"
#include "llvm/Support/Compiler.h"
#include "llvm/Target/TargetMachine.h"
#include "llvm/TargetParser/Host.h"
#include "llvm/Passes/PassBuilder.h"
#include "llvm/Passes/OptimizationLevel.h"
#include "llvm/MC/TargetRegistry.h"

#include "antlr4-runtime.h"
#include "FirstTyrLexer.h"
#include "FirstTyrParser.h"
#include "CustomFirstTyrVisitor.h"

#include "AST/RootNode.h"
#include "ASTVisitor.h"

std::tuple<int, int, int> parse_semver(const std::string &filename);
std::shared_ptr<RootNode> parse_file(const std::string &filename);
void run_tests(std::tuple<int, int, int> compilerVersion);
void compile_file(const std::string &filename);