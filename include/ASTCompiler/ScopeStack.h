#ifndef SCOPE_STACK_H
#define SCOPE_STACK_H

#include "llvm/IR/Function.h"
#include "llvm/IR/Value.h"
#include <unordered_map>
#include <vector>
#include <string>
#include <iostream>
#include "Symbol.h"

class ScopeStack
{
public:
    void pushScope();
    void popScope();
    void addSymbol(const Symbol &symbol);
    Symbol *getSymbol(const std::string &name);
    Symbol *getCurrentScopeSymbol(const std::string &name);
    void listSymbols() const;

private:
    std::vector<std::unordered_map<std::string, Symbol>> scopes;
};

#endif // SCOPE_STACK_H
