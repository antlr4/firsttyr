#pragma once

#include "llvm/IR/Module.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Verifier.h"

#include "AST/AllASTNodes.h"

#include "ScopeStack.h"

class ASTVisitor
{
public:
    ASTVisitor(const RootNode &root);
    std::unique_ptr<llvm::Module> generate();

    // Visit functions

    TypedValue visit(const AdditionNode &node);
    TypedValue visit(const ArrayNode &node);
    TypedValue visit(const ArrayIndexingNode &node);
    Symbol visit(const ArgNode &node);
    std::vector<Symbol> visit(const ArgsNode &node);
    void visit(const AssignmentNode &node);
    void visit(const BlockBodyNode &node);
    TypedValue visit(const ComparisonNode &node);
    TypedValue visit(const EqualityNode &node);
    TypedValue visit(const ExpressionNode &node);
    void visit(const FileNode &node);
    TypedValue visit(const FunctionCallNode &node);
    void visit(const FunctionDeclNode &node);
    TypedValue visit(const IdentifierNode &node);
    void visit(const IfStatementNode &node);
    TypedValue visit(const IntLiteralNode &node);
    TypedValue visit(const FloatLiteralNode &node);
    TypedValue visit(const BooleanLiteralNode &node);
    TypedValue visit(const StringLiteralNode &node);
    TypedValue visit(const MultiplicationNode &node);
    TypedValue visit(const UnaryNode &node);
    TypedValue visit(const TypeCastNode &node);
    std::pair<std::string, TypedValue> visit(const ParamNode &node);
    std::map<std::string, TypedValue> visit(const ParamsNode &node);
    void visit(const ReturnStatementNode &node);
    void visit(const StatementNode &node);

    // Helper functions

    llvm::Module *getModule()
    {
        return module.get();
    }

    // Scope stack functions

    void pushScope()
    {
        scopeStack.pushScope();
    }

    void popScope()
    {
        scopeStack.popScope();
    }

    void addSymbol(const Symbol &symbol)
    {
        scopeStack.addSymbol(symbol);
    }

    Symbol *getSymbol(const std::string &name)
    {
        return scopeStack.getSymbol(name);
    }

    Symbol *getCurrentScopeSymbol(const std::string &name)
    {
        return scopeStack.getCurrentScopeSymbol(name);
    }

    // Type conversion functions

    SymbolType getSymbolTypeFromTypeString(const std::string &typeString);

    SymbolType getSymbolTypeFromLLVMType(llvm::Type *type);

    llvm::Type *getLLVMTypeFromTypeString(const std::string &typeString);

    llvm::Type *getLLVMTypeFromSymbolType(const SymbolType &symbolType, const SymbolType &elementType = SymbolType::Void, uint64_t arraySize = 0);

    llvm::Type *getLLVMTypeFromNodeType(const NodeType &nodeType);

    std::string getTypeStringFromSymbolType(const SymbolType &symbolType);

    std::string getTypeStringFromNodeType(const NodeType &nodeType);

    std::string getTypeStringFromLLVMType(llvm::Type *type);

    // Type casting functions

    llvm::Value *intToString(llvm::Value *value);

    llvm::Value *floatToString(llvm::Value *value);

    llvm::Value *boolToString(llvm::Value *value);

    llvm::Value *stringToInt(llvm::Value *value);

    llvm::Value *stringToFloat(llvm::Value *value);

    llvm::Value *stringToBool(llvm::Value *value);

    llvm::Value *intToFloat(llvm::Value *value);

    llvm::Value *floatToInt(llvm::Value *value);

    llvm::Value *intToBool(llvm::Value *value);

    llvm::Value *floatToBool(llvm::Value *value);

    llvm::Value *boolToInt(llvm::Value *value);

    llvm::Value *boolToFloat(llvm::Value *value);

    // External function getters

    llvm::Function *getMallocFunc()
    {
        llvm::Function *mallocFunc = module->getFunction("malloc");
        if (!mallocFunc)
        {
            std::vector<llvm::Type *> mallocArgs(1, llvm::Type::getInt64Ty(context));
            llvm::FunctionType *mallocType = llvm::FunctionType::get(llvm::Type::getInt8PtrTy(context), mallocArgs, false);
            mallocFunc = llvm::Function::Create(mallocType, llvm::Function::ExternalLinkage, "malloc", module.get());
        }
        return mallocFunc;
    }

    llvm::Function *getSprintfFunc()
    {
        llvm::Function *sprintfFunc = module->getFunction("sprintf");
        if (!sprintfFunc)
        {
            std::vector<llvm::Type *> sprintfArgs(2, llvm::Type::getInt8PtrTy(context));
            llvm::FunctionType *sprintfType = llvm::FunctionType::get(llvm::Type::getInt32Ty(context), sprintfArgs, true);
            sprintfFunc = llvm::Function::Create(sprintfType, llvm::Function::ExternalLinkage, "sprintf", module.get());
        }
        return sprintfFunc;
    }

    llvm::Function *getStrcpyFunc()
    {
        llvm::Function *strcpyFunc = module->getFunction("strcpy");
        if (!strcpyFunc)
        {
            std::vector<llvm::Type *> strcpyArgs(2, llvm::Type::getInt8PtrTy(context));
            llvm::FunctionType *strcpyType = llvm::FunctionType::get(llvm::Type::getInt8PtrTy(context), strcpyArgs, false);
            strcpyFunc = llvm::Function::Create(strcpyType, llvm::Function::ExternalLinkage, "strcpy", module.get());
        }
        return strcpyFunc;
    }

    llvm::Function *getStrcatFunc()
    {
        llvm::Function *strcatFunc = module->getFunction("strcat");
        if (!strcatFunc)
        {
            std::vector<llvm::Type *> strcatArgs(2, llvm::Type::getInt8PtrTy(context));
            llvm::FunctionType *strcatType = llvm::FunctionType::get(llvm::Type::getInt8PtrTy(context), strcatArgs, false);
            strcatFunc = llvm::Function::Create(strcatType, llvm::Function::ExternalLinkage, "strcat", module.get());
        }
        return strcatFunc;
    }

    llvm::Function *getStrlenFunc()
    {
        llvm::Function *strlenFunc = module->getFunction("strlen");
        if (!strlenFunc)
        {
            std::vector<llvm::Type *> strlenArgs(1, llvm::Type::getInt8PtrTy(context));
            llvm::FunctionType *strlenType = llvm::FunctionType::get(llvm::Type::getInt64Ty(context), strlenArgs, false);
            strlenFunc = llvm::Function::Create(strlenType, llvm::Function::ExternalLinkage, "strlen", module.get());
        }
        return strlenFunc;
    }

    llvm::Function *getStrcmpFunc()
    {
        llvm::Function *strcmpFunc = module->getFunction("strcmp");
        if (!strcmpFunc)
        {
            std::vector<llvm::Type *> strcmpArgs(2, llvm::Type::getInt8PtrTy(context));
            llvm::FunctionType *strcmpType = llvm::FunctionType::get(llvm::Type::getInt32Ty(context), strcmpArgs, false);
            strcmpFunc = llvm::Function::Create(strcmpType, llvm::Function::ExternalLinkage, "strcmp", module.get());
        }
        return strcmpFunc;
    }

    llvm::Function *getPrintfFunc()
    {
        llvm::Function *printfFunc = module->getFunction("printf");
        if (!printfFunc)
        {
            std::vector<llvm::Type *> printfArgs(1, llvm::Type::getInt8PtrTy(context));
            llvm::FunctionType *printfType = llvm::FunctionType::get(llvm::Type::getInt32Ty(context), printfArgs, true);
            printfFunc = llvm::Function::Create(printfType, llvm::Function::ExternalLinkage, "printf", module.get());
        }
        return printfFunc;
    }

private:
    llvm::LLVMContext context;
    std::unique_ptr<llvm::Module> module;
    llvm::IRBuilder<> builder;
    const RootNode &root;
    ScopeStack scopeStack;
};
