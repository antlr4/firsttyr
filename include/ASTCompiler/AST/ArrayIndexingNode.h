#pragma once

#include "ASTNode.h"
#include "ExpressionNode.h"
#include "IdentifierNode.h"
#include <memory>

class ArrayIndexingNode : public ASTNode
{
public:
    ArrayIndexingNode(FirstTyrParser::ArrayIndexingContext *ctx, const std::string &tokenText, std::shared_ptr<IdentifierNode> array, std::shared_ptr<ExpressionNode> index)
        : ASTNode(NodeType::ArrayIndexingNode, ctx, tokenText), array(std::move(array)), index(std::move(index))
    {
    }

    const std::shared_ptr<IdentifierNode> &getArray() const
    {
        return array;
    }

    const std::shared_ptr<ExpressionNode> &getIndex() const
    {
        return index;
    }

private:
    std::shared_ptr<IdentifierNode> array;
    std::shared_ptr<ExpressionNode> index;
};