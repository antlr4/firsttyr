#pragma once

#include "ASTNode.h"
#include "ExpressionNode.h"
#include <memory>
#include <string>

enum class UnaryOperator
{
    SUB,
};

class UnaryNode : public ASTNode
{
public:
    UnaryNode(std::shared_ptr<ExpressionNode> operand, UnaryOperator op, bool isPrefix, FirstTyrParser::UnaryContext *ctx, const std::string &tokenText)
        : ASTNode(NodeType::UnaryNode, ctx, tokenText), operand(std::move(operand)), operatorType(op), isPrefix(isPrefix) {}

    std::shared_ptr<ExpressionNode> getOperand() const { return operand; }
    UnaryOperator getOperatorType() const { return operatorType; }
    bool getIsPrefix() const { return isPrefix; }

private:
    std::shared_ptr<ExpressionNode> operand;
    UnaryOperator operatorType;
    bool isPrefix;
};
