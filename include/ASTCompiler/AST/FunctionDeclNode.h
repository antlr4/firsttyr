#pragma once

#include "ASTNode.h"
#include "ParamsNode.h"
// #include "BlockBodyNode.h" // This is a circular dependency
#include "ReturnStatementNode.h"
#include <string>
#include <vector>
#include <memory>

class BlockBodyNode; // Forward declaration

class FunctionDeclNode : public ASTNode
{
public:
    FunctionDeclNode(const std::string &name, const std::string &type, FirstTyrParser::FunctionDeclContext *ctx, const std::string &tokenText)
        : ASTNode(NodeType::FunctionDeclNode, ctx, tokenText), name(name), returnType(type) {}

    void setParameters(std::shared_ptr<ParamsNode> paramsNode)
    {
        parameters = std::move(paramsNode);
    }

    void setBlockBody(std::shared_ptr<BlockBodyNode> blockBodyNode)
    {
        blockBody = std::move(blockBodyNode);
    }

    void setReturnStatement(std::shared_ptr<ReturnStatementNode> returnStmtNode)
    {
        returnStatement = std::move(returnStmtNode);
    }

    const std::string &getName() const
    {
        return name;
    }

    const std::string &getReturnType() const
    {
        return returnType;
    }

    const std::shared_ptr<ParamsNode> &getParameters() const
    {
        return parameters;
    }

    const std::shared_ptr<BlockBodyNode> &getBlockBody() const
    {
        return blockBody;
    }

private:
    std::string name;
    std::string returnType;
    std::shared_ptr<ParamsNode> parameters;
    std::shared_ptr<BlockBodyNode> blockBody;
    std::shared_ptr<ReturnStatementNode> returnStatement; // Updated to use ReturnStatementNode
};
