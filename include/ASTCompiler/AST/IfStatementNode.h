#pragma once

#include "ASTNode.h"
#include "ExpressionNode.h"
// #include "BlockBodyNode.h" // This is a circular dependency
#include <memory>

class BlockBodyNode; // Forward declaration

class IfStatementNode : public ASTNode
{
public:
    IfStatementNode(std::vector<std::pair<std::shared_ptr<ExpressionNode>, std::shared_ptr<BlockBodyNode>>> conditionBlockPairs, std::shared_ptr<BlockBodyNode> elseBlockBody, FirstTyrParser::IfStatementContext *ctx, const std::string &tokenText)
        : ASTNode(NodeType::IfStatementNode, ctx, tokenText), conditionBlockPairs(std::move(conditionBlockPairs)), elseBlockBody(std::move(elseBlockBody)) {}

    const std::vector<std::pair<std::shared_ptr<ExpressionNode>, std::shared_ptr<BlockBodyNode>>> &getConditionBlockPairs() const { return conditionBlockPairs; }
    std::shared_ptr<BlockBodyNode> getElseBlockBody() const { return elseBlockBody; }

private:
    std::vector<std::pair<std::shared_ptr<ExpressionNode>, std::shared_ptr<BlockBodyNode>>> conditionBlockPairs;
    std::shared_ptr<BlockBodyNode> elseBlockBody;
};