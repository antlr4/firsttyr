#pragma once

#include "ASTNode.h"
#include <memory>

class ExpressionNode : public ASTNode
{
public:
    ExpressionNode(std::shared_ptr<ASTNode> node, antlr4::ParserRuleContext *ctx, const std::string &tokenText)
        : ASTNode(NodeType::ExpressionNode, ctx, tokenText), node(std::move(node)) {}

    std::shared_ptr<ASTNode> getExpression() const { return node; }

private:
    std::shared_ptr<ASTNode> node;
};