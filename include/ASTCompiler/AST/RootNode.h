#pragma once

#include "ASTNode.h"
#include "FileNode.h"
#include <vector>
#include <set>

class RootNode : public ASTNode
{
public:
    RootNode() : ASTNode() {}
    ~RootNode() = default;

    void addFile(std::shared_ptr<FileNode> file)
    {
        files.push_back(std::move(file));
    }

    const std::vector<std::shared_ptr<FileNode>> &getFiles() const
    {
        return files;
    }

    void addStandardFunction(const std::string &functionName)
    {
        standardFunctions.insert(functionName);
    }

    const std::set<std::string> &getStandardFunctions() const
    {
        return standardFunctions;
    }

private:
    std::vector<std::shared_ptr<FileNode>> files;
    std::set<std::string> standardFunctions;
};