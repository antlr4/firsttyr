#pragma once

#include "ASTNode.h"
#include "AssignmentNode.h"
#include "FunctionDeclNode.h"
#include "FunctionCallNode.h"
#include "ReturnStatementNode.h"
#include "IfStatementNode.h"
#include <variant>
#include <memory>

class StatementNode : public ASTNode
{
public:
    StatementNode(std::shared_ptr<ASTNode> statement, FirstTyrParser::StatementContext *ctx, const std::string &tokenText)
        : ASTNode(NodeType::StatementNode, ctx, tokenText), statement(std::move(statement)) {}

    const std::shared_ptr<ASTNode> &getStatement() const { return statement; }

private:
    std::shared_ptr<ASTNode> statement;
};
