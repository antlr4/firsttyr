#pragma once

#include "ASTNode.h"
#include "ExpressionNode.h"
#include <memory>
#include <string>

enum class AdditionOperator
{
    ADD,
    SUB,
};

class AdditionNode : public ASTNode
{
public:
    AdditionNode(std::shared_ptr<ExpressionNode> left, AdditionOperator op, std::shared_ptr<ExpressionNode> right, FirstTyrParser::AdditionContext *ctx, const std::string &tokenText)
        : ASTNode(NodeType::AdditionNode, ctx, tokenText), leftOperand(std::move(left)), operatorType(op), rightOperand(std::move(right)) {}

    std::shared_ptr<ExpressionNode> getLeftOperand() const { return leftOperand; }
    AdditionOperator getOperatorType() const { return operatorType; }
    std::shared_ptr<ExpressionNode> getRightOperand() const { return rightOperand; }

private:
    std::shared_ptr<ExpressionNode> leftOperand;
    AdditionOperator operatorType;
    std::shared_ptr<ExpressionNode> rightOperand;
};
