#pragma once

#include "ASTNode.h"
#include "ParamNode.h"
#include <vector>
#include <string>

class ParamsNode : public ASTNode
{
public:
    ParamsNode(FirstTyrParser::ParamsContext *ctx, const std::string &tokenText)
        : ASTNode(NodeType::ParamsNode, ctx, tokenText) {}

    void addParam(const std::shared_ptr<ParamNode> &param)
    {
        paramNodes.push_back(param);
    }
    std::vector<std::shared_ptr<ParamNode>> getParams() const
    {
        return paramNodes;
    }

private:
    std::vector<std::shared_ptr<ParamNode>> paramNodes;
};