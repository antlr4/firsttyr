#pragma once

#include "ASTNode.h"
#include "ArgNode.h"
#include <vector>
#include <memory>

class ArgsNode : public ASTNode
{
public:
    ArgsNode(FirstTyrParser::ArgsContext *ctx, const std::string &tokenText)
        : ASTNode(NodeType::ArgsNode, ctx, tokenText) {}

    void addArg(const std::shared_ptr<ArgNode> &arg)
    {
        argNodes.push_back(arg);
    }
    std::vector<std::shared_ptr<ArgNode>> getArgs() const
    {
        return argNodes;
    }

private:
    std::vector<std::shared_ptr<ArgNode>> argNodes;
};