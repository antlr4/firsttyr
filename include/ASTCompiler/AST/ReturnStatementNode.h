// ReturnStatementNode.h
#pragma once

#include "ASTNode.h"
#include "ExpressionNode.h"
#include <memory>

class ReturnStatementNode : public ASTNode
{
public:
    ReturnStatementNode(std::shared_ptr<ExpressionNode> returnExpression, FirstTyrParser::ReturnStatementContext *ctx, const std::string &tokenText)
        : ASTNode(NodeType::ReturnStatementNode, ctx, tokenText), returnExpression(std::move(returnExpression)) {}

    std::shared_ptr<ExpressionNode> getReturnExpression() const { return returnExpression; }

private:
    std::shared_ptr<ExpressionNode> returnExpression;
};
