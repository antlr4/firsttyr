#pragma once

#include "ASTNode.h"
#include <string>
#include <memory>

enum class AssignmentType
{
    Mutable,
    Immutable
};

class AssignmentNode : public ASTNode
{
public:
    AssignmentNode(AssignmentType type, const std::string &identifier, std::shared_ptr<ASTNode> value, FirstTyrParser::AssignmentContext *ctx, const std::string &tokenText)
        : ASTNode(NodeType::AssignmentNode, ctx, tokenText), assignmentType(type), identifier(identifier), value(std::move(value)) {}

    AssignmentType getAssignmentType() const
    {
        return assignmentType;
    }

    const std::string &getIdentifier() const
    {
        return identifier;
    }

    std::shared_ptr<ASTNode> getValue() const
    {
        return value;
    }

private:
    AssignmentType assignmentType;
    std::string identifier;
    std::shared_ptr<ASTNode> value;
};
