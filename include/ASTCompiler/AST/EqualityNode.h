#pragma once

#include "ASTNode.h"
#include "ExpressionNode.h"
#include <memory>

enum class EqualityOperator
{
    EQ,
    NE,
};

class EqualityNode : public ASTNode
{
public:
    EqualityNode(std::shared_ptr<ExpressionNode> left, EqualityOperator op, std::shared_ptr<ExpressionNode> right, FirstTyrParser::ExpressionContext *ctx, const std::string &tokenText)
        : ASTNode(NodeType::EqualityNode, ctx, tokenText), leftOperand(std::move(left)), operatorType(op), rightOperand(std::move(right)) {}

    std::shared_ptr<ExpressionNode> getLeftOperand() const { return leftOperand; }
    EqualityOperator getOperatorType() const { return operatorType; }
    std::shared_ptr<ExpressionNode> getRightOperand() const { return rightOperand; }

private:
    std::shared_ptr<ExpressionNode> leftOperand;
    EqualityOperator operatorType;
    std::shared_ptr<ExpressionNode> rightOperand;
};
