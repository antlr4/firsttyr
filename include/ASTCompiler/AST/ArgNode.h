#pragma once

#include "ASTNode.h"
#include "ExpressionNode.h"
#include <string>
#include <memory>

class ArgNode : public ASTNode
{
public:
    ArgNode(const std::string &key, std::shared_ptr<ExpressionNode> expression, FirstTyrParser::ArgContext *ctx, const std::string &tokenText)
        : ASTNode(NodeType::ArgNode, ctx, tokenText), identifier(key), expression(std::move(expression)) {}

    const std::string &getIdentifier() const
    {
        return identifier;
    }

    std::shared_ptr<ExpressionNode> getExpression() const
    {
        return expression;
    }

private:
    std::string identifier;
    std::shared_ptr<ExpressionNode> expression;
};
