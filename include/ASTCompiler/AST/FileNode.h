#pragma once

#include "ASTNode.h"
#include "FunctionDeclNode.h"
#include <vector>

class FileNode : public ASTNode
{
public:
    FileNode(FirstTyrParser::FileContext *ctx, const std::string &tokenText)
        : ASTNode(NodeType::FileNode, ctx, tokenText) {}
    ~FileNode() = default;

    void addFunctionDecl(std::shared_ptr<FunctionDeclNode> functionDecl)
    {
        functionDecls.push_back(std::move(functionDecl));
    }

    const std::vector<std::shared_ptr<FunctionDeclNode>> &getFunctionDecls() const
    {
        return functionDecls;
    }

    void setFilename(const std::string &name)
    {
        filename = name;
    }

    const std::string &getFilename() const
    {
        return filename;
    }

    void setFullFilePath(const std::string &path)
    {
        fullFilePath = path;
    }

    const std::string &getFullFilePath() const
    {
        return fullFilePath;
    }

private:
    std::vector<std::shared_ptr<FunctionDeclNode>> functionDecls;
    std::string filename;
    std::string fullFilePath;
};