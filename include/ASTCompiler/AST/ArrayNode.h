#pragma once

#include "ASTNode.h"
#include "ExpressionNode.h"
#include <memory>

class ArrayNode : public ASTNode
{
public:
    ArrayNode(FirstTyrParser::ArrayDeclarationContext *ctx, const std::string &tokenText, std::vector<std::shared_ptr<ExpressionNode>> elements)
        : ASTNode(NodeType::ArrayNode, ctx, tokenText), elements(std::move(elements))
    {
        if (!this->elements.empty())
        {
            // Infer the type from the first element
            this->type = this->elements[0]->getExpression()->getNodeType();
        }
    }

    const std::vector<std::shared_ptr<ExpressionNode>> &getElements() const
    {
        return elements;
    }

    const NodeType &getType() const
    {
        return type;
    }

private:
    std::vector<std::shared_ptr<ExpressionNode>> elements;
    NodeType type;
};