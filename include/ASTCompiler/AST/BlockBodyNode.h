#pragma once

#include "ASTNode.h"
// #include "StatementNode.h" // This is a circular dependency
#include <vector>
#include <memory>

class StatementNode; // Forward declaration

class BlockBodyNode : public ASTNode
{
public:
    BlockBodyNode(FirstTyrParser::BlockBodyContext *ctx, const std::string &tokenText)
        : ASTNode(NodeType::BlockBodyNode, ctx, tokenText) {}

    void addStatement(std::shared_ptr<StatementNode> statement)
    {
        statements.push_back(std::move(statement));
    }

    const std::vector<std::shared_ptr<StatementNode>> &getStatements() const
    {
        return statements;
    }

private:
    std::vector<std::shared_ptr<StatementNode>> statements;
};