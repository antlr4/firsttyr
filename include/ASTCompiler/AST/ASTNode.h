#pragma once

#include <iostream>
#include <memory>
#include "FirstTyrParser.h"

enum class NodeType
{
    AdditionNode,
    ArrayNode,
    ArrayIndexingNode,
    ArgNode,
    ArgsNode,
    AssignmentNode,
    BooleanLiteralNode,
    BlockBodyNode,
    ComparisonNode,
    EqualityNode,
    ExpressionNode,
    FileNode,
    FloatLiteralNode,
    FunctionCallNode,
    FunctionDeclNode,
    IdentifierNode,
    IfStatementNode,
    IntLiteralNode,
    LiteralNode,
    MultiplicationNode,
    ParamNode,
    ParamsNode,
    ReturnStatementNode,
    RootNode,
    StatementNode,
    StringLiteralNode,
    TypeCastNode,
    UnaryNode
};

class ASTNode : public std::enable_shared_from_this<ASTNode>
{
public:
    ASTNode()
        : nodeType(NodeType::RootNode), tokenText(""), tokenPosition(-1, -1) {}

    ASTNode(NodeType type, antlr4::ParserRuleContext *ctx, const std::string &tokenText)
        : nodeType(type), tokenText(tokenText), tokenPosition(ctx->getStart()->getLine(), ctx->getStart()->getCharPositionInLine()) {}
    virtual ~ASTNode() = default;

    NodeType getNodeType() const
    {
        return nodeType;
    }

    std::string getTokenText() const
    {
        return tokenText;
    }

    std::pair<int, int> getTokenPosition() const
    {
        return tokenPosition;
    }

private:
    const NodeType nodeType;

protected:
    const std::string tokenText;
    const std::pair<int, int> tokenPosition;
};
