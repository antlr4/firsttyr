#pragma once

#include "ASTNode.h"
#include <memory>

class LiteralNode : public ASTNode
{
public:
    LiteralNode(NodeType type, FirstTyrParser::LiteralContext *ctx, const std::string &tokenText)
        : ASTNode(type, ctx, tokenText) {}
    virtual ~LiteralNode() = default;
};

class IntLiteralNode : public LiteralNode
{
public:
    IntLiteralNode(int value, FirstTyrParser::LiteralContext *ctx, const std::string &tokenText)
        : LiteralNode(NodeType::IntLiteralNode, ctx, tokenText), value(value) {}

    int getValue() const
    {
        return value;
    }

private:
    int value;
};

class FloatLiteralNode : public LiteralNode
{
public:
    FloatLiteralNode(float value, FirstTyrParser::LiteralContext *ctx, const std::string &tokenText)
        : LiteralNode(NodeType::FloatLiteralNode, ctx, tokenText), value(value) {}

    float getValue() const
    {
        return value;
    }

private:
    float value;
};

class BooleanLiteralNode : public LiteralNode
{
public:
    BooleanLiteralNode(bool value, FirstTyrParser::LiteralContext *ctx, const std::string &tokenText)
        : LiteralNode(NodeType::BooleanLiteralNode, ctx, tokenText), value(value) {}

    bool getValue() const
    {
        return value;
    }

private:
    bool value;
};

class StringLiteralNode : public LiteralNode
{
public:
    StringLiteralNode(const std::string &value, FirstTyrParser::LiteralContext *ctx, const std::string &tokenText)
        : LiteralNode(NodeType::StringLiteralNode, ctx, tokenText), value(value) {}

    const std::string &getValue() const
    {
        return value;
    }

private:
    std::string value;
};