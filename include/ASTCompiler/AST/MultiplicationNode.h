#pragma once

#include "ASTNode.h"
#include "ExpressionNode.h"
#include <memory>

enum class MultiplicationOperator
{
    MUL,
    DIV,
};

class MultiplicationNode : public ASTNode
{
public:
    MultiplicationNode(std::shared_ptr<ExpressionNode> left, MultiplicationOperator op, std::shared_ptr<ExpressionNode> right, FirstTyrParser::MultiplicationContext *ctx, const std::string &tokenText)
        : ASTNode(NodeType::MultiplicationNode, ctx, tokenText), leftOperand(std::move(left)), operatorType(op), rightOperand(std::move(right)) {}

    std::shared_ptr<ExpressionNode> getLeftOperand() const { return leftOperand; }
    MultiplicationOperator getOperatorType() const { return operatorType; }
    std::shared_ptr<ExpressionNode> getRightOperand() const { return rightOperand; }

private:
    std::shared_ptr<ExpressionNode> leftOperand;
    MultiplicationOperator operatorType;
    std::shared_ptr<ExpressionNode> rightOperand;
};
