#pragma once

#include "ASTNode.h"
#include "ExpressionNode.h"
#include <memory>
#include <string>

class TypeCastNode : public ASTNode
{
public:
    TypeCastNode(std::shared_ptr<ExpressionNode> operand, const std::string &type, FirstTyrParser::TypeCastContext *ctx, const std::string &tokenText)
        : ASTNode(NodeType::TypeCastNode, ctx, tokenText), operand(std::move(operand)), type(type) {}

    std::shared_ptr<ExpressionNode> getOperand() const { return operand; }
    std::string getType() const { return type; }

private:
    std::shared_ptr<ExpressionNode> operand;
    std::string type;
};