#pragma once

#include "ASTNode.h"
#include <string>

class IdentifierNode : public ASTNode
{
public:
    IdentifierNode(const std::string &identifier, FirstTyrParser::IdentifierContext *ctx, const std::string &tokenText)
        : ASTNode(NodeType::IdentifierNode, ctx, tokenText), identifier(identifier) {}

    const std::string &getIdentifier() const
    {
        return identifier;
    }

private:
    std::string identifier;
};
