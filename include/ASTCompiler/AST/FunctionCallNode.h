#pragma once

#include "ASTNode.h"
#include "ArgsNode.h"
#include <string>
#include <vector>
#include <memory>

class FunctionCallNode : public ASTNode
{
public:
    FunctionCallNode(const std::string &functionName, FirstTyrParser::FunctionCallContext *ctx, const std::string &tokenText)
        : ASTNode(NodeType::FunctionCallNode, ctx, tokenText), functionName(functionName) {}

    const std::string &getFunctionName() const
    {
        return functionName;
    }

    const std::shared_ptr<ArgsNode> &getArguments() const
    {
        return argsNode;
    }

    void setArguments(std::shared_ptr<ArgsNode> newArgsNode)
    {
        argsNode = std::move(newArgsNode);
    }

private:
    std::string functionName;
    std::shared_ptr<ArgsNode> argsNode;
};
