#pragma once

#include "ASTNode.h"
#include "ExpressionNode.h"
#include <memory>

enum class ComparisonOperator
{
    LT,
    LE,
    GT,
    GE,
};

class ComparisonNode : public ASTNode
{
public:
    ComparisonNode(std::shared_ptr<ExpressionNode> left, ComparisonOperator op, std::shared_ptr<ExpressionNode> right, FirstTyrParser::ComparisonContext *ctx, const std::string &tokenText)
        : ASTNode(NodeType::ComparisonNode, ctx, tokenText), leftOperand(std::move(left)), operatorType(op), rightOperand(std::move(right)) {}

    std::shared_ptr<ExpressionNode> getLeftOperand() const { return leftOperand; }
    ComparisonOperator getOperatorType() const { return operatorType; }
    std::shared_ptr<ExpressionNode> getRightOperand() const { return rightOperand; }

private:
    std::shared_ptr<ExpressionNode> leftOperand;
    ComparisonOperator operatorType;
    std::shared_ptr<ExpressionNode> rightOperand;
};
