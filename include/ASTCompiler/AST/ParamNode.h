#pragma once

#include "ASTNode.h"
#include <string>

class ParamNode : public ASTNode
{
public:
    ParamNode(const std::string &name, const std::string &type, FirstTyrParser::ParamContext *ctx, const std::string &tokenText)
        : ASTNode(NodeType::ParamNode, ctx, tokenText), name(name), paramType(type) {}

    const std::string &getName() const { return name; }
    const std::string &getType() const { return paramType; }

private:
    std::string name;
    std::string paramType;
};