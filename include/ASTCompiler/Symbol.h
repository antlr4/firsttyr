#pragma once

#include <string>
#include <map>
#include <llvm/IR/Value.h>

enum class SymbolType
{
    Array,
    Integer,
    Float,
    Function,
    Boolean,
    String,
    Void,
    UNKNOWN,
    // ... add other types as needed
};

struct Symbol
{
    std::string name;
    SymbolType type;
    bool isMutable;
    llvm::Value *value;
    // Array attributes
    SymbolType elementType; // type of the elements in the array
    int arraySize;          // size of the array
    // Function attributes
    llvm::Type *functionReturnType;
    std::map<std::string, int> functionParameterIndices;
    std::map<std::string, SymbolType> functionParameterTypes;
    bool isFunctionParameter;
};

struct TypedValue
{
    SymbolType type;
    llvm::Value *value;

    TypedValue() : type(SymbolType::UNKNOWN), value(nullptr) {}

    TypedValue(SymbolType type, llvm::Value *value)
        : type(type), value(value) {}
};