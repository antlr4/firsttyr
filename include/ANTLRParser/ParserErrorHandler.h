#pragma once
#include <string>
#include <vector>
#include <antlr4-runtime.h>
#include "TypeHelpers.h"

class ParserErrorHandler
{
public:
    enum class ErrorType
    {
        // ANTLR parser errors
        InvalidContextError,
        SyntaxError,
        UnexpectedTypeError,
        BrokenFileError,
    };

    class Error
    {
    public:
        Error(const std::string &message, int line, int column, ErrorType errorType)
            : message(message), line(line), column(column), errorType(errorType) {}

        int getLine() const { return line; }
        int getColumn() const { return column; }
        std::string getMessage() const { return message; }
        ErrorType getErrorType() const { return errorType; }

    private:
        std::string message;
        int line;
        int column;
        ErrorType errorType;
    };

    void addInvalidContextError(const std::string &message, antlr4::ParserRuleContext *ctx)
    {
        addError("Invalid Context: " + message, ctx->getStart()->getLine(), ctx->getStart()->getCharPositionInLine(), ErrorType::InvalidContextError);
    }

    void addSyntaxError(const std::string &message, antlr4::ParserRuleContext *ctx)
    {
        addError("Undefined Syntax: " + message, ctx->getStart()->getLine(), ctx->getStart()->getCharPositionInLine(), ErrorType::SyntaxError);
    }

    void addUnexpectedTypeError(const std::type_info &actualType, const std::string &expectedTypeName, antlr4::ParserRuleContext *ctx)
    {
        std::string message = "Expected " + expectedTypeName + ", got " + getTypeName(actualType);
        addError("Unexpected Type: " + message, ctx->getStart()->getLine(), ctx->getStart()->getCharPositionInLine(), ErrorType::UnexpectedTypeError);
    }

    void addBrokenFileError(const std::string &fileName)
    {
        std::cout << "Adding broken file error" << std::endl;
        addError("Broken File: " + fileName, 0, 0, ErrorType::BrokenFileError);
    }

    const std::vector<Error> &getErrors() const { return errors; }

    bool hasErrors() const { return !errors.empty(); }

private:
    std::vector<Error> errors;

    void addError(const std::string &message, int line, int column, ErrorType errorType)
    {
        errors.emplace_back(message, line, column, errorType);
    }
};