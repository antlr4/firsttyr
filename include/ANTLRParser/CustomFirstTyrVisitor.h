#pragma once

#include <vector>
#include <unordered_map>
#include "antlr4-runtime.h"

#include "FirstTyrVisitor.h"
#include "FirstTyrParser.h"
#include "ParserErrorHandler.h"
#include "ANTLRHelpers.h"

#include "AST/AllASTNodes.h"

class CustomFirstTyrVisitor : public FirstTyrVisitor
{
public:
    CustomFirstTyrVisitor(FirstTyrParser &parser, antlr4::CommonTokenStream &tokens, std::shared_ptr<RootNode> astRoot);

    antlrcpp::Any visitFile(FirstTyrParser::FileContext *ctx) override;
    antlrcpp::Any visitFunctionDecl(FirstTyrParser::FunctionDeclContext *ctx) override;
    antlrcpp::Any visitParams(FirstTyrParser::ParamsContext *ctx) override;
    antlrcpp::Any visitParam(FirstTyrParser::ParamContext *ctx) override;
    antlrcpp::Any visitBlockBody(FirstTyrParser::BlockBodyContext *ctx) override;
    antlrcpp::Any visitStatement(FirstTyrParser::StatementContext *ctx) override;
    antlrcpp::Any visitAssignment(FirstTyrParser::AssignmentContext *ctx) override;
    antlrcpp::Any visitReturnStatement(FirstTyrParser::ReturnStatementContext *ctx) override;
    antlrcpp::Any visitIfStatement(FirstTyrParser::IfStatementContext *ctx) override;
    antlrcpp::Any visitElseIfPart(FirstTyrParser::ElseIfPartContext *ctx) override;
    antlrcpp::Any visitElsePart(FirstTyrParser::ElsePartContext *ctx) override;
    antlrcpp::Any visitExpression(FirstTyrParser::ExpressionContext *ctx) override;
    antlrcpp::Any visitComparison(FirstTyrParser::ComparisonContext *ctx) override;
    antlrcpp::Any visitAddition(FirstTyrParser::AdditionContext *ctx) override;
    antlrcpp::Any visitMultiplication(FirstTyrParser::MultiplicationContext *ctx) override;
    antlrcpp::Any visitUnary(FirstTyrParser::UnaryContext *ctx) override;
    antlrcpp::Any visitTypeCast(FirstTyrParser::TypeCastContext *ctx) override;
    antlrcpp::Any visitAtom(FirstTyrParser::AtomContext *ctx) override;
    antlrcpp::Any visitArrayIndexing(FirstTyrParser::ArrayIndexingContext *ctx) override;
    antlrcpp::Any visitArrayDeclaration(FirstTyrParser::ArrayDeclarationContext *ctx) override;
    antlrcpp::Any visitLiteral(FirstTyrParser::LiteralContext *ctx) override;
    antlrcpp::Any visitArg(FirstTyrParser::ArgContext *ctx) override;
    antlrcpp::Any visitArgs(FirstTyrParser::ArgsContext *ctx) override;
    antlrcpp::Any visitFunctionCall(FirstTyrParser::FunctionCallContext *ctx) override;
    antlrcpp::Any visitIdentifier(FirstTyrParser::IdentifierContext *ctx) override;

    void populateAST(antlr4::tree::ParseTree *tree, const std::string &filename, const std::string &fullFilePath)
    {
        // Visit the parse tree and get the FileNode
        antlrcpp::Any result = visit(tree);
        if (result.type() == typeid(std::shared_ptr<FileNode>))
        {
            auto fileNode = std::any_cast<std::shared_ptr<FileNode>>(result);

            // Set the filename and full file path
            fileNode->setFilename(filename);
            fileNode->setFullFilePath(fullFilePath);

            // Add the FileNode to the AST root
            astRoot->addFile(fileNode);
        }
        else
        {
            errorHandler.addBrokenFileError(filename);
        }
    }

    std::shared_ptr<RootNode> &getAST() { return astRoot; }

    ParserErrorHandler &getParserErrorHandler() { return errorHandler; }

private:
    FirstTyrParser &parser;
    antlr4::CommonTokenStream &tokens;

    std::shared_ptr<RootNode> astRoot;
    ParserErrorHandler errorHandler;
};
