#pragma once
#include <string>
#include "antlr4-runtime.h"

std::string getTextIncludingHiddenTokens(antlr4::ParserRuleContext *ctx, antlr4::CommonTokenStream &tokens);
