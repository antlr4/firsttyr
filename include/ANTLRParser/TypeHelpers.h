#pragma once
#include <typeinfo>
#include <string>
#include <regex>

#include "AST/AllASTNodes.h"

std::string getTypeName(const std::type_info &typeInfo);
std::string codePointToUtf8(uint32_t codePoint);