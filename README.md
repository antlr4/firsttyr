# FirstTyr

## How to run
0. You may need to set up ANTLR4, LLVM, Cmake and GCC (skip after first setup):
   1. Install libraries:
      1. `brew install antlr`
      2. `brew install llvm`
      3. `brew install cmake`
      4. `brew install gcc`
   2. Update paths:
      1. Add Homebrew's installation of `llvm/bin` to your `PATH` in your `.bash_profile` or `.zshrc` file: `export PATH="/opt/homebrew/opt/llvm/bin:$PATH"`
      2. Update `CMakeLists.txt`'s `set(LLVM_DIR, "...")` to the correct `.../lib/cmake/llvm` path
   3. Generate ANTLR4 parser files (repeat after any changes to `FirstTyr.g4`):
      1. `pip install antlr4-tools`
      2. `cd src`
      3. `antlr4 -Dlanguage=Cpp -visitor -o parser FirstTyr.g4`
      4. `cd ..`
1. `cd build`
2. Run `cmake .. && make && ./firsttyr` (for `main.ftyr`) or `cmake .. && make && ./firsttyr file.ftyr`
3. To compile, run `clang -O2 -o main main.ll && ./main` (replace `main` with name of `.ftyr` file)

## How to test
0. (See above)
1. `cd build`
2. (Optional) Make files in `tests/` with the following filename format: `{x.y.z}-{test-name}.ftyr` to extend tests
3. Run `cmake .. && make && ./firsttyr --test`

## Debugging
If you encounter build errors like a broken `CMAKE_OSX_SYSROOT` path, which may happen in cases like when updating your Xcode version, delete the contents of the `/build/` folder and rerun the build steps above.

## About
This is the beginning of a toy programming language meant to gain experience with the intricacies of ANTLR4 and LLVM.

For example, the following example syntax is enabled (see `src/main.ftyr` for the latest iteration, and `tests/` for previous versions):

```ftyr
int main = () {
  int fibonacci = (n: int) {
    if (n <= 1) {
      return n
    }
    return fibonacci(n = n - 1) + fibonacci(n = n - 2)
  }
  return fibonacci(n = 9) // 34
}
```

This compiles to the following LLVM IR:

```llvm
; ModuleID = 'FirstTyrModule'
source_filename = "FirstTyrModule"

define i32 @main() {
entry:
  %fibonacci = call i32 @fibonacci(i32 9)
  ret i32 %fibonacci
}

define i32 @fibonacci(i32 %n) {
entry:
  %le = icmp sle i32 %n, 1
  br i1 %le, label %then, label %merge

then:                                             ; preds = %entry
  ret i32 %n

merge:                                            ; preds = %entry
  %sub = sub i32 %n, 1
  %fibonacci = call i32 @fibonacci(i32 %sub)
  %sub1 = sub i32 %n, 2
  %fibonacci2 = call i32 @fibonacci(i32 %sub1)
  %add = add i32 %fibonacci, %fibonacci2
  ret i32 %add
}
```

The following features are enabled (most recent first):
- 0.1.6:
  - Custom parser error handler
- 0.1.5:
  - Added type conversion
- 0.1.4:
  - Print function for text
- 0.1.3:
  - Unary operator for negative numbers
- 0.1.2:
  - String type (with concatenation and equality comparison)
- 0.1.1:
  - "else if" and "else"
- 0.1.0:
  - Refactored to build up a type-safe Abstract Syntax Tree
- 0.0.10:
  - Conditional "if" statements
- 0.0.9:
  - Boolean comparison operators (==, !=, <, <=, >, >=)
- 0.0.8:
  - Boolean type
- 0.0.7:
  - Function parameters
  - Comments
- 0.0.6:
  - Function calls
- 0.0.5:
  - File level scope
  - Nested function declarations
- 0.0.4:
  - Constant and mutable variables
  - Constant variables by default
- 0.0.3:
  - Complex integer and float arithmetics
  - Arithmetics using variables
  - Conversion from integer to float on division
- 0.0.2:
  - Block scope and variable assignments
- 0.0.1:
  - Function declarations
  - 32-bit integers and floats
  - Function return types (int, float, none (void))